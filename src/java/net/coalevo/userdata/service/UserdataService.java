/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.service;

import net.coalevo.foundation.model.*;
import net.coalevo.userdata.model.EditableUserdata;
import net.coalevo.userdata.model.Userdata;
import net.coalevo.userdata.model.UserdataPrivacy;
import net.coalevo.userdata.model.UserdataServiceException;

import java.util.NoSuchElementException;
import java.util.List;

/**
 * Provides access to {@link Userdata} that is associated with
 * a {@link AgentIdentifier}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface UserdataService
    extends Service, Restoreable, Maintainable {

  /**
   * Begin to create new userdata.
   * This method should be followed by either {@link #cancelUserdataTransaction(Agent,EditableUserdata)}
   * or {@link #commitUserdata(Agent,EditableUserdata)}.
   *
   * @param caller the calling {@link Agent}.
   * @param aid    an {@link AgentIdentifier}.
   * @return a {@link EditableUserdata} instance.
   * @throws SecurityException if the calling {@link Agent} is not authentic or authorized.
   * @throws UserdataServiceException if userdata for the specified identifier is already
   *                                  available from this service.
   * @see #cancelUserdataTransaction(Agent,EditableUserdata)
   * @see #commitUserdata(Agent,EditableUserdata)
   */
  public EditableUserdata beginUserdataCreate(Agent caller, AgentIdentifier aid)
      throws SecurityException, UserdataServiceException;

  /**
   * Begin to update existing userdata.
   * This method should be followed by either {@link #cancelUserdataTransaction(Agent,EditableUserdata)}
   * or {@link #commitUserdata(Agent,EditableUserdata)}.
   *
   * @param caller the calling {@link Agent}.
   * @param aid    an {@link AgentIdentifier}.
   * @return a {@link EditableUserdata} instance.
   * @throws SecurityException if the calling {@link Agent} is not authentic or authorized.
   * @throws UserdataServiceException if userdata for the specified identifier is not available from this service
   *                                  (e.g. needs to be created).
   * @see #cancelUserdataTransaction(Agent,EditableUserdata)
   * @see #commitUserdata(Agent,EditableUserdata)
   */
  public EditableUserdata beginUserdataUpdate(Agent caller, AgentIdentifier aid)
      throws SecurityException, UserdataServiceException;

  /**
   * Cancels a userdata transaction.
   * This method may be called after {@link #beginUserdataCreate(Agent,AgentIdentifier)},
   * or {@link #beginUserdataUpdate(Agent,AgentIdentifier)}.
   *
   * @param caller the calling {@link Agent}.
   * @param eud    the {@link EditableUserdata} instance obtained when the transaction was started.
   * @throws SecurityException     if the calling {@link Agent} is not authentic or authorized.
   * @throws IllegalStateException if the transaction was not started with the corresponding
   *                               {@link #beginUserdataCreate(Agent,AgentIdentifier)}
   *                               or {@link #beginUserdataUpdate(Agent,AgentIdentifier)}.
   * @see #beginUserdataCreate(Agent,AgentIdentifier)
   * @see #beginUserdataUpdate(Agent,AgentIdentifier)
   */
  public void cancelUserdataTransaction(Agent caller, EditableUserdata eud)
      throws SecurityException, IllegalStateException;

  /**
   * Commits an edited {@link EditableUserdata} instance.
   * <p/>
   * This method may be called after {@link #beginUserdataCreate(Agent,AgentIdentifier)},
   * or {@link #beginUserdataUpdate(Agent,AgentIdentifier)}.
   * </p>
   * <p/>
   * Note that the implementation should treat email updates with a
   * verification mechanism. The idea is to send a mail to the specified
   * address, and validate the new email when {@link #confirmEmail(Agent,AgentIdentifier,String)}
   * is called.
   * </p>
   * <p/>
   * The implementation should also mark userdata on updates to user related data
   * as non-reviewed. This allows some type of moderating of user profiles.
   * </p>
   * <p/>
   * The implementation may have a delay until updated userdata is re-indexed with
   * the new values.
   * </p>
   *
   * @param caller the calling {@link Agent}.
   * @param eud    an {@link EditableUserdata} instance.
   * @throws SecurityException     if the calling {@link Agent} is not authentic or authorized.
   * @throws IllegalStateException if the transaction was not started with the corresponding
   *                               {@link #beginUserdataCreate(Agent,AgentIdentifier)}
   *                               or {@link #beginUserdataUpdate(Agent,AgentIdentifier)}.
   * @throws UserdataServiceException if an internal error occurs.
   */
  public void commitUserdata(Agent caller, EditableUserdata eud)
      throws SecurityException, IllegalStateException, UserdataServiceException;

  /**
   * Removes the {@link Userdata} instance bound to the given
   * {@link AgentIdentifier}.
   * <p/>
   * The implementation may have a delay until removed userdata disappears
   * from search indices.
   * </p>
   *
   * @param caller the calling {@link Agent}.
   * @param aid    {@link AgentIdentifier} instance.
   * @throws NoSuchElementException if no {@link Userdata} is associated with
   *                                the given {@link AgentIdentifier}.
   * @throws SecurityException      if the calling {@link Agent} is not authentic or
   *                                not authorized.
   */
  public void removeUserdata(Agent caller, AgentIdentifier aid)
      throws SecurityException;

  /**
   * Confirms the mail for Userdata bound to a given {@link AgentIdentifier}.
   *
   * @param caller  the calling {@link Agent}.
   * @param aid     an {@link AgentIdentifier}.
   * @param confuid the confirmation unique identifier that was sent per mail.
   * @return true if confirmed, false otherwise.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @throws UserdataServiceException if there is no pending confirmation for the given
   *                                  agent identifier or the update operation fails.
   */
  public boolean confirmEmail(Agent caller, AgentIdentifier aid, String confuid)
      throws SecurityException, UserdataServiceException;

  /**
   * Generates a new confirmation identifier and sends email.
   * <p>
   * Note that changing the e-mail in the userdata will always send a confirmation
   * request, so this is only for cases where there was some unexpected problem with
   * the confirmation.
   * </p>
   *
   * @param caller the calling {@link Agent}.
   * @param aid an {@link AgentIdentifier}.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public void retryConfirmation(Agent caller, AgentIdentifier aid)
      throws SecurityException;

  /**
   * Marks userdata as reviewed.
   *
   * @param caller the calling {@link Agent}.
   * @param aid    an {@link AgentIdentifier}.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @throws UserdataServiceException if the update operation fails.
   */
  public void markReviewed(Agent caller, AgentIdentifier aid)
      throws SecurityException, UserdataServiceException;

  /**
   * Tests if {@link Userdata} is available for a given
   * {@link AgentIdentifier}.
   *
   * @param caller the calling {@link Agent}.
   * @param aid    an      {@link AgentIdentifier}.
   * @return true if available, false otherwise.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public boolean isUserdataAvailable(Agent caller, AgentIdentifier aid)
      throws SecurityException;

  /**
   * Returns the {@link Userdata} instance associated with
   * the given {@link AgentIdentifier}.
   *
   * @param caller the calling {@link Agent}.
   * @param aid    an      {@link AgentIdentifier}.
   * @return a {@link Userdata} instance.
   * @throws NoSuchElementException if no {@link Userdata} is associated with
   *                                the given {@link AgentIdentifier}.
   * @throws SecurityException      if the calling {@link Agent} is not authentic or
   *                                not authorized.
   */
  public Userdata getUserdata(Agent caller, AgentIdentifier aid)
      throws SecurityException;

  /**
   * Returns the {@link Userdata} instances associated with
   * the given nickname.
   * <p/>
   * Note: This method will only return public {@link Userdata} instances.
   * Public means that all fields indicated as private by the associated
   * {@link UserdataPrivacy} will be nulled out and the instance is not mutable.
   * If you need to obtain private instances (mutable and all fields) then
   * you should resolve nicknames into identifiers first and then call
   * {@link #getUserdata(Agent,AgentIdentifier)}.
   * </p>
   *
   * @param caller   the calling {@link Agent}.
   * @param nickname a nickname.
   * @return an array of {@link Userdata} instances.
   * @throws NoSuchElementException if no {@link Userdata} is associated with
   *                                the given {@link AgentIdentifier}.
   * @throws SecurityException      if the calling {@link Agent} is not authentic or
   *                                not authorized.
   */
  public Userdata[] getUserdata(Agent caller, String nickname)
      throws SecurityException;

  /**
   * Lists the {@link AgentIdentifier} instances of userdata entries
   * that have not yet been reviewed, since creation or update.
   *
   * @param caller the calling {@link Agent}.
   * @return an array of {@link AgentIdentifier} instances or null if
   *         query could not be parsed or not entry matched.
   */
  public List<AgentIdentifier> listUnreviewedUserdata(Agent caller);

  /**
   * Lists the {@link AgentIdentifier} instances of userdata entries
   * that have no confirmed email address, since creation or update.
   *
   * @param caller the calling {@link Agent}.
   * @return an array of {@link AgentIdentifier} instances or null if
   *         query could not be parsed or not entry matched.
   */
  public List<AgentIdentifier> listUnconfirmedUserdata(Agent caller);

  /**
   * Returns the nickname for a {@link AgentIdentifier}.
   * <p/>
   * This is a convenience method that should aid in implementing
   * systems with a unique nickname (used as well for login).
   * </p>
   *
   * @param caller the calling {@link Agent}.
   * @param a      {@link AgentIdentifier} instance.
   * @return the nickname.
   * @throws NoSuchElementException if no {@link Userdata} is associated with
   *                                the given {@link AgentIdentifier}.
   * @throws SecurityException      if the calling {@link Agent} is not authentic or
   *                                not authorized.
   */
  public String getNickname(Agent caller, AgentIdentifier a)
      throws SecurityException;

  /**
   * Tests if a given Nickname exists in the services store.
   *
   * @param caller   the calling {@link Agent}.
   * @param nickname a nickname.
   * @return true if exists, false otherwise.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public boolean existsNickname(Agent caller, String nickname)
      throws SecurityException;

  /**
   * Tests if a given nickname is valid.
   *
   * @param caller   the calling {@link Agent}.
   * @param nickname a nickname.
   * @return true if valid, false otherwise.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public boolean isValidNickname(Agent caller, String nickname)
      throws SecurityException;

  /**
   * Tests if a given agent identifier is valid.
   *
   * @param caller   the calling {@link Agent}.
   * @param aid an agent identifier string.
   * @return true if valid, false otherwise.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public boolean isValidAgentIdentifier(Agent caller, String aid)
      throws SecurityException;

  /**
   * Tests if a given Email exists in the services store.
   * @param caller  the calling {@link Agent}.
   * @param email  the email.
   * @return true if exists, false otherwise.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public boolean existsEmail(Agent caller, String email)
      throws SecurityException;

  /**
   * Returns the email for a given agent.
   * @param caller the calling {@link Agent}.
   * @param aid  an {@link AgentIdentifier} instance
   * @return the email.
   */
  public String getEmail(Agent caller, AgentIdentifier aid);

  /**
   * Returns the identifiers for entries with the given nickname
   * in the services store.
   *
   * @param caller   the calling {@link Agent}.
   * @param nickname a nickname.
   * @return an array of {@link AgentIdentifier} instances.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public AgentIdentifier[] getIdentifiers(Agent caller, String nickname)
      throws SecurityException;

  /**
   * Search for userdata matching a given query.
   * <p/>
   * Note: The query language is Lucene.
   * </p>
   *
   * @param caller the calling {@link Agent}.
   * @param query  the query <tt>String</tt>.
   * @return an array of {@link AgentIdentifier} instances or null if
   *         query could not be parsed or not entry matched.
   */
  public AgentIdentifier[] searchUserdata(Agent caller, String query);

  /**
   * Search for public userdata matching a given query.
   * <p/>
   * Note: The query language is Lucene.
   * </p>
   *
   * @param caller the calling {@link Agent}.
   * @param query  the query <tt>String</tt>.
   * @return an array of {@link AgentIdentifier} instances or null if
   *         query could not be parsed or not entry matched.
   */
  public AgentIdentifier[] searchPublicUserdata(Agent caller, String query);

  /**
   * Rebuilds the userdata indices.
   *
   * @param caller the calling {@link Agent}.
   * @throws UserdataServiceException if the index rebuilding failed.
   */
  public void rebuildIndices(Agent caller)
      throws UserdataServiceException;

}//interface UserdataService
