/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.service;

import net.coalevo.userdata.model.Userdata;
import net.coalevo.userdata.model.UserdataPrivacy;
import net.coalevo.foundation.model.Service;

import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;

/**
 * Instances of this service provide
 * mapping between XML and {@link Userdata} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface UserdataXMLService extends Service {

  /**
   * Serializes the given {@link Userdata} instance
   * as XML to the given writer.
   * <p/>
   * If <tt>filter</tt> is true, data flagged as private
   * in the associated {@link UserdataPrivacy} instance
   * will be filtered and wont appear in the XML output.
   * </p>
   *
   * @param w      a <tt>Writer</tt>.
   * @param d      a {@link Userdata} instance.
   * @param filter flags if private data should be filtered.
   * @throws IOException if the I/O fails.
   */
  public void toXML(Writer w, Userdata d, boolean filter)
      throws IOException;

  /**
   * Serializes the given {@link Userdata} instance
   * as XML to the given <tt>OutputStream</tt>.
   * <p/>
   * If <tt>filter</tt> is true, data flagged as private
   * in the associated {@link UserdataPrivacy} instance
   * will be filtered and wont appear in the XML output.
   * </p>
   *
   * @param out      an <tt>OutputStream</tt>.
   * @param d      a {@link Userdata} instance.
   * @param filter flags if private data should be filtered.
   * @throws IOException if the I/O fails.
   */
  public void toXML(OutputStream out, Userdata d, boolean filter)
      throws IOException;

  /**
   * Convenience method that will return the serialized
   * {@link Userdata} instance as XML in a <tt>String</tt>.
   *
   * @param d      a {@link Userdata} instance.
   * @param filter flags if private data should be filtered.
   * @return the marshalled {@link Userdata} instance as String.
   * @throws IOException if the I/O fails.
   * @see #toXML(Writer,Userdata,boolean)
   */
  public String toXML(Userdata d, boolean filter) throws IOException;

  /**
   * Serializes the given {@link Userdata} instance
   * as XML to the given stream writer.
   *
   * @param w      a <tt>XMLStreamWriter</tt>.
   * @param ud      a {@link Userdata} instance.
   * @param filter flags if private data should be filtered.
   * @throws XMLStreamException if a stream I/O error occurs.
   */
  public void toXML(XMLStreamWriter w, Userdata ud, boolean filter)
      throws XMLStreamException;

  /**
   * Reads an XML serialized {@link Userdata} instance from a
   * given <tt>Reader</tt>.
   *
   * @param r a <tt>Reader</tt>.
   * @return the unserialized {@link Userdata} instance.
   * @throws IOException if the I/O fails.
   */
  public Userdata fromXML(Reader r) throws IOException;

  /**
   * Reads an XML serialized {@link Userdata} instance from a
   * given <tt>InputStream</tt>.
   *
   * @param in a <tt>InputStream</tt>.
   * @return the unserialized {@link Userdata} instance.
   * @throws IOException if the I/O fails.
   */
  public Userdata fromXML(InputStream in) throws IOException;

  /**
   * Convenience method that reads an XML serialized {@link Userdata}
   * instance from a given <tt>String</tt>.
   *
   * @param str a <tt>String</tt> containing the XML.
   * @return the unserialized {@link Userdata} instance.
   * @throws IOException if the I/O fails.
   */
  public Userdata fromXML(String str) throws IOException;

   /**
   * Reads an XML serialized {@link Userdata} instance from a
   * given <tt>XMLStreamReader</tt>.
   *
   * @param reader a <tt>Reader</tt>.
   * @return the unserialized {@link Userdata} instance.
   * @throws XMLStreamException if a stream I/O error occurs.
   */
  public Userdata fromXML(XMLStreamReader reader) throws XMLStreamException;

}//interface UserdataXMLService
