/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.service;

/**
 * Defines a tag interface for the registration of the
 * unified bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 * @see net.coalevo.foundation.util.BundleConfiguration
 */
public interface UserdataConfiguration {

  //Userdata
  public static final String DATA_SOURCE_KEY = "datasource";
  public static final String CONNECTIONPOOLSIZE_KEY = "connections.poolsize";
  public static final String USERDATACACHESIZE_KEY = "cache.userdata";
  public static final String NICKNAMECACHESIZE_KEY = "cache.nicknames";
  public static final String CONFIRM_EXPIRY_KEY = "confirmation.expiry";
  public static final String CONFIRM_WSBASEURLKEY_KEY = "confirmation.wsbaseurl";
  public static final String CONFIRM_POLICYURL_KEY = "confirmation.policyurl";
  public static final String CONFIRM_SYSNAME_KEY = "confirmation.sysname";


  public static final String VALIDATION_NICK_PATTERN_KEY = "validation.nick.pattern";
  public static final String VALIDATION_AID_PATTERN_KEY = "validation.aid.pattern"; 

  //UserdataXMLService
  public static final String XMPARSERPOOL_MAXACTIVE_KEY = "xmlparser.pool.maxactive";
  public static final String XMPARSERPOOL_MAXIDLE_KEY = "xmlparser.pool.maxidle";
  public static final String XMPARSERPOOL_MINIDLE_KEY = "xmlparser.pool.minidle";
  public static final String XMPARSERPOOL_MAXWAIT_KEY = "xmlparser.pool.maxwait";
  public static final String XMPARSERPOOL_EXHAUSTACTION_KEY = "xmlparser.pool.exhaustaction";

  public static final String POOLEXHAUST_GROW = "grow";
  public static final String POOLEXHAUST_FAIL = "fail";
  public static final String POOLEXHAUST_BLOCK = "block";

}//interface UserdataConfiguration
