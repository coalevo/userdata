/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.model;

/**
 * Defines the access privileges for a {@link Userdata}
 * instance.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface UserdataPrivacy {

  /**
   * Tests if the prefix can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsPrefix();

  /**
   * Tests if the firstnames can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsFirstnames();

  /**
   * Tests if the additional names can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsAdditionalnames();

  /**
   * Tests if the lastnames can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsLastnames();

  /**
   * Tests if the suffix can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsSuffix();

  /**
   * Tests if the birthdate can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsBirthdate();

  /**
   * Tests if the gender can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsGender();

  /**
   * Tests if the street can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsStreet();

  /**
   * Tests if the extension can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsExtension();

  /**
   * Tests if the city can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsCity();

  /**
   * Tests if the country can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsCountry();

  /**
   * Tests if the postal code can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsPostalCode();

  /**
   * Tests if the email can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsEmail();

  /**
   * Tests if the phone can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsPhone();

  /**
   * Tests if the mobile can be accessed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean getAllowsMobile();

}//interface UserdataPrivacy
