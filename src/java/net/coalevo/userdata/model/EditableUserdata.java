/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.model;

import java.util.Date;

/**
 * Defines the contract for <tt>EditableUserdata</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface EditableUserdata
    extends Userdata {

  /**
   * Returns a {@link EditableUserdataPrivacy} that encapsulates
   * access bits for the userdata (privat vs. public).
   *
   * @return an {@link EditableUserdataPrivacy} instance.
   */
  public EditableUserdataPrivacy getEditablePrivacy();

  /**
   * Sets the first login date
   *
   * @param date the first login date.
   */
  public void setFirstLoginDate(Date date);

  /**
   * Sets the nickname.
   * @param nickname as <tt>String</tt>.
   */
  public void setNickname(String nickname);

  /**
   * Sets the gender.
   *
   * @param gender should be {@link #GENDER_MALE}, {@link #GENDER_FEMALE},
   *        or {@link #GENDER_UNKNOWN}.
   */
  public void setGender(String gender);

  /**
   * Sets the mime <tt>image</tt> subtype of the associated photo
   * @param mimetype the mimetype of the photo. (jpeg,png,etc.)
   */
  public void setPhotoType(String mimetype);

  /**
   * Sets the Base64 encoded photo.
   *
   * @param photo the photo encoded as Base64 <tt>String</tt>.
   */
  public void setPhoto(String photo);

  /**
   * Sets the arbitrary info text.
   * @param info an informative text as <tt>String</tt>.
   */
  public void setInfo(String info);

  /**
   * Sets the info text format identifier.
   * @param infoformat a format identifier.
   */
  public void setInfoFormat(String infoformat);
  
  /**
   * Sets arbitrary annotations.
   *
   * @param ann annotations text as <tt>String</tt>.
   */
  public void setAnnotations(String ann);

  /**
   * Sets the prefix for the name, that
   * might be of academical, official or other nature.
   * <p>
   * e.g. Dr.
   * </p>
   * @param prefix the prefix as <tt>String</tt>.
   */
  public void setPrefix(String prefix);

  /**
   * Sets the suffix for the name, that
   * might be of academical, official or other nature.
   * <p>
   * e.g. MD
   * </p>
   * @param suffix the suffix as <tt>String</tt>.
   */
  public void setSuffix(String suffix);

  /**
   * Sets the additional names.
   *
   * @param additionalNames the additonal names.
   */
  public void setAdditionalNames(String additionalNames);

  /**
   * Sets the firstnames.
   *
   * @param firstnames the firstnames.
   */
  public void setFirstnames(String firstnames);

  /**
   * Sets the lastnames.
   *
   * @param lastnames the lastnames.
   */
  public void setLastnames(String lastnames);

  /**
   * Sets the birthdate.
   *
   * @param birthdate the birthdate.
   */
  public void setBirthdate(Date birthdate);

  /**
   * Sets the street.
   *
   * @param street the street.
   */
  public void setStreet(String street);

  /**
   * Sets the extension for the address.
   *
   * @param extension the extendion for the address.
   */
  public void setExtension(String extension);

  /**
   * Sets the city.
   * @param city the city.
   */
  public void setCity(String city);

  /**
   * Sets the country.
   *
   * @param country the country as ISO Standard 2 Letter code.
   */
  public void setCountry(String country);

  /**
   * Sets the postal code.
   * @param postalCode the postal code.
   */
  public void setPostalCode(String postalCode);

  /**
   * Sets the eMail.
   * @param email an email.
   */
  public void setEmail(String email);

  /**
   * Sets the link type.
   * @param linkType the link type as {@link #LINK_BLOG}, {@link #LINK_HOMEPAGE}
   * or {@link #LINK_OTHER}.
   */
   public void setLinkType(String linkType);

  /**
   * Sets the Link URL.
   * @param link the link URL as <tt>String</tt>.
   */
  public void setLink(String link);

  /**
   * Sets the phone.
   *
   * @param phone the phone as <tt>String</tt>.
   */
  public void setPhone(String phone);

  /**
   * Sets the mobile phone.
   * @param mobile the mobile phone as <tt>String</tt>.
   */
  public void setMobile(String mobile);

  /**
   * Sets the languages of this user.
   * <p>
   * The <em>first</em> item in this list will be treated
   * as the default language. (i.e. there must be at least one.)
   * </p>
   * @param languages a comma separated list of languages given by their
   *         ISO Standard code.
   */
  public void setLanguages(String languages);

  /**
   * Adds a flag.
   * @param flag the flag as <tt>String</tt>.
   */
  public void addFlag(String flag);

  /**
   * Updates the flag at the given index.
   * @param idx the index starting from 1.
   * @param flag the flag.
   */
  public void updateFlag(int idx, String flag);

  /**
   * Removes a flag.
   *
   * @param flag a flag as <tt>String</tt>.
   */
  public void removeFlag(String flag);

  /**
   * Removes a flag at a given index.
   * @param idx the flag at the index, counting from 1.
   * @return true if removed, false otherwise.
   */
  public boolean removeFlag(int idx);

  /**
   * Clears all flags.
   */
  public void clearFlags();

}//interface Userdata
