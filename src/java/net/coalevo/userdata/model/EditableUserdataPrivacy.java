/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.model;

/**
 * This interface defines an editable {@link UserdataPrivacy}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface EditableUserdataPrivacy
    extends UserdataPrivacy {

  /**
   * Sets the flag that determines if the prefix can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsPrefix(boolean b);

  /**
   * Sets the flag that determines  if the firstnames can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsFirstnames(boolean b);

  /**
   * Sets the flag that determines  if the additional names can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsAdditionalnames(boolean b);

  /**
   * Sets the flag that determines  if the lastnames can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsLastnames(boolean b);

  /**
   * Sets the flag that determines  if the suffix can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsSuffix(boolean b);

  /**
   * Sets the flag that determines  if the birthdate can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsBirthdate(boolean b);

  /**
   * Sets the flag that determines  if the gender can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsGender(boolean b);

  /**
   * Sets the flag that determines  if the street can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsStreet(boolean b);

  /**
   * Sets the flag that determines  if the extension can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsExtension(boolean b);

  /**
   * Sets the flag that determines  if the city can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsCity(boolean b);

  /**
   * Sets the flag that determines  if the country can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsCountry(boolean b);

  /**
   * Sets the flag that determines  if the postal code can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsPostalCode(boolean b);

  /**
   * Sets the flag that determines  if the email can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsEmail(boolean b);

  /**
   * Sets the flag that determines  if the phone can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsPhone(boolean b);

  /**
   * Sets the flag that determines  if the mobile can be accessed.
   *
   * @param b true if allowed, false otherwise.
   */
  public void setAllowsMobile(boolean b);

}//interface EditableUserdataPrivacy
