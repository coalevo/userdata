/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.model;

import net.coalevo.foundation.model.Identifiable;

import java.util.Date;
import java.util.Iterator;

/**
 * Defines the contract for <tt>Userdata</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Userdata
    extends Identifiable {

  /**
   * Returns a {@link UserdataPrivacy} that encapsulates
   * access bits for the userdata (privat vs. public).
   *
   * @return a {@link UserdataPrivacy} instance.
   */
  public UserdataPrivacy getPrivacy();

  /**
   * Returns the first login date.
   *
   * @return the first login date.
   */
  public Date getFirstLoginDate();

  /**
   * Returns the nickname.
   *
   * @return the nickname as <tt>String</tt>.
   */
  public String getNickname();

  /**
   * Returns the gender.
   *
   * @return the gender as <tt>String</tt> that is either
   *        {@link #GENDER_MALE}, {@link #GENDER_FEMALE},
   *        or {@link #GENDER_UNKNOWN}.
   */
  public String getGender();

  /**
   * Tests if this <tt>Userdata</tt> contains a photo.
   * @return true if contains photo, false otherwise.
   */
  public boolean hasPhoto();

  /**
   * Returns the mime <tt>image</tt> subtype of the associated
   * photo.
   * <p>
   * e.g. <tt>png</tt> or <tt>jpg</tt> (the type is always <tt>image</tt>).
   * </p>
   * @return the associated photo's Mime type.
   * @see #getPhoto
   */
  public String getPhotoType();

  /**
   * Returns the photo as a Base64 encoded <tt>String</tt>.
   *
   * @return a Base64 encoded image.
   * @see #getPhotoType()
   */
  public String getPhoto();

  /**
   * Returns an arbitrary info text.
   *
   * @return an info text as <tt>String</tt>.
   */
  public String getInfo();

  /**
   * Returns the info format identifier.
   * @return the info text format identifier as <tt>String</tt>.
   */
  public String getInfoFormat();

  /**
   * Returns arbitrary annotions.
   *
   * @return an annotations text as <tt>String</tt>.
   */
  public String getAnnotations();

  /**
   * Returns the prefix for the name, that
   * might be of academical, official or other nature.
   * <p>
   * e.g. Dr.
   * </p>
   * @return the prefix as <tt>String</tt>.
   */
  public String getPrefix();

  /**
   * Returns the suffix for the name, that
   * might be of academical, official or other nature.
   * <p>
   * e.g. MD
   * </p>
   * @return the suffix as <tt>String</tt>.
   */
  public String getSuffix();

  /**
   * Returns the additional names.
   *
   * @return the additional names.
   */
  public String getAdditionalnames();

  /**
   * Returns the firstnames.
   * @return the firstnames.
   */
  public String getFirstnames();

  /**
   * Returns the lastnames.
   *
   * @return the lastnames.
   */
  public String getLastnames();

  /**
   * Returns the birthdate.
   *
   * @return the birthdate.
   */
  public Date getBirthdate();

  /**
   * Returns the street.
   *
   * @return the street.
   */
  public String getStreet();

  /**
   * Returns an extension for the address.
   *
   * @return the extension for the address.
   */
  public String getExtension();

  /**
   * Returns the city.
   *
   * @return the city.
   */
  public String getCity();

  /**
   * Returns the country.
   *
   * @return the country as ISO Standard 2 Letter code.
   */
  public String getCountry();

  /**
   * Returns the postal code.
   *
   * @return the postal code.
   */
  public String getPostalCode();

  /**
   * Returns the eMail.
   *
   * @return the eMail.
   */
  public String getEmail();

  /**
   * Returns the link type.
   * @return the link type as {@link #LINK_BLOG}, {@link #LINK_HOMEPAGE}
   * or {@link #LINK_OTHER}.
   */
  public String getLinkType();

  /**
   * Returns the Link URL.
   * @return the link URL as <tt>String</tt>.
   */
  public String getLink();

  /**
   * Returns the phone.
   * @return the phone as <tt>String</tt>.
   */
  public String getPhone();

  /**
   * Returns the mobile phone.
   * @return the mobile phone as <tt>String</tt>.
   */
  public String getMobile();

  /**
   * Returns the languages of this user.
   * <p>
   * The <em>first</em> item in this list is the default
   * language.
   * </p>
   * @return a comma separated list of languages given by their
   *         ISO Standard code.
   */
  public String getLanguages();

  /**
   * Returns all flags.
   *
   * @return an <tt>Iterator</tt> over <tt>String</tt> instances.
   */
  public Iterator<String> getFlags();

  /**
   * Returns the flag at the given index.
   * @param idx the index of the flag, from 1.
   * @return the flag.
   */
  public String getFlag(int idx);

  /**
   * Returns the number of flags.
   * @return the number of flags.
   */
  public int getNumberOfFlags();

  /**
   * Tests if the email of this instance was confirmed.
   * @return true if confirmed, false otherwise.
   */
  public boolean isConfirmed();

  /**
   * Tests if this instance was reviewed.
   * @return true if reviewed, false otherwise.
   */
  public boolean isReviewed();

  /**
   * Tests if this instance is public.
   * @return true if public, false otherwise.
   */
  public boolean isPublic();

  public static final String LINK_HOMEPAGE = "homepage";
  public static final String LINK_BLOG = "blog";
  public static final String LINK_OTHER = "other";

  public static final String GENDER_MALE = "m";
  public static final String GENDER_FEMALE = "f";
  public static final String GENDER_UNKNOWN = "u";

}//interface Userdata
