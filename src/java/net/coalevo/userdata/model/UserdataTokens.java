/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.model;

/**
 * Provides constants that define the tokens used within
 * a userdata XML document.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UserdataTokens {

  public static final String VERSION = "1.0";
  public static final String NS_PREFIX="cud";
  public static final String PI_TARGET = "xml-stylesheet";
  public static final String PI_DATA="type=\"text/xsl\" href=\"userdata-xhtml.xsl\"";
  public static final String NAMESPACE="http://www.coalevo.net/schemas/userdata-v10";
  public static final String XSI_NS_PREFIX = "xsi";
  public static final String XSI_NAMESPACE="http://www.w3.org/2001/XMLSchema-instance";
  public static final String XSI_SCHEMALOCATION_ATTR = "schemaLocation";
  public static final String XSI_SCHEMALOCATION = "http://www.coalevo.net/schemas/userdata-v10\n" +
      "    http://www.coalevo.net/schemas/userdata-v10.xsd";

  public static final String ELEMENT_USERDATA = "userdata";
  public static final String ATTR_USERDATA_VERSION = "version";
  public static final String ATTR_USERDATA_UID ="uid";
  public static final String ATTR_CONFIRMED="confirmed";
  public static final String ATTR_REVIEWED="reviewed";

  public static final String ATTR_HIDDEN = "hidden";

  public static final String ELEMENT_NICKNAME = "nickname";

  //personal
  public static final String ELEMENT_PERSONAL = "personal";
  public static final String ELEMENT_PREFIX = "prefix";
  public static final String ELEMENT_FIRSTNAME = "firstnames";
  public static final String ELEMENT_ADDITIONALNAME = "additionalnames";
  public static final String ELEMENT_LASTNAME = "lastnames";
  public static final String ELEMENT_SUFFIX = "suffix";
  public static final String ELEMENT_BIRTHDATE = "birthdate";
  public static final String ELEMENT_GENDER = "gender";

  //address
  public static final String ELEMENT_ADDRESS = "address";
  public static final String ELEMENT_STREET = "street";
  public static final String ELEMENT_EXTENSION = "extension";
  public static final String ELEMENT_CITY = "city";
  public static final String ELEMENT_COUNTRY = "country";
  public static final String ELEMENT_POSTALCODE = "postalcode";

  //communications
  public static final String ELEMENT_COMMUNICATIONS = "communications";
  public static final String ELEMENT_EMAIL = "email";
  public static final String ELEMENT_LINK = "link";
  public static final String ELEMENT_PHONE = "phone";
  public static final String ELEMENT_MOBILE = "mobile";
  public static final String ELEMENT_LANGUAGES = "languages";

  public static final String ELEMENT_PHOTO = "photo";
  public static final String ATTR_TYPE = "type";

  public static final String ELEMENT_INFO = "info";
  public static final String ATTR_FORMAT = "format";
  public static final String ELEMENT_ANNOTATIONS="annotations";
  public static final String ELEMENT_FLAGS = "flags";
  public static final String ELEMENT_FLAG = "flag";

  public static final String ELEMENT_FIRSTLOGINDATE = "firstlogin";
  public static final String NO_NAMESPACE ="";

  public static final String XMLDOC_HEADER = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
//  public static final String USERDATA_START_PRE =
//      "<cud:userdata xmlns:cud=\"http://www.coalevo.net/schemas/userdata-v10\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.coalevo.net/schemas/userdata-v10\n" +
//      "              http://www.coalevo.net/schemas/userdata-v10.xsd\" version=\"1.0\" uid=\"";
  public static final String USERDATA_START_PRE =
      "<userdata xmlns=\"http://www.coalevo.net/schemas/userdata-v10\" version=\"1.0\" uid=\"";

  public static final String USERDATA_START_POST = "\">";
  public static final String USERDATA_END = "</userdata>";

  public static final String START_NICKNAME = "<nickname>";
  public static final String END_NICKNAME = "</nickname>";

  public static final String ENTITIES = "<!DOCTYPE userdata [\n" +
      "  <!ENTITY Auml \"&#196;\">\n" +
      "  <!ENTITY auml \"&#228;\">\n" +
      "  <!ENTITY Ouml \"&#214;\">\n" +
      "  <!ENTITY ouml \"&#246;\">\n" +
      "  <!ENTITY Uuml \"&#220;\">\n" +
      "  <!ENTITY uuml \"&#252;\">\n" +
      "]>\n";

}//class UserdataTokens
