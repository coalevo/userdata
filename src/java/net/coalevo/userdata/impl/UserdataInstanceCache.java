/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.foundation.model.BaseIdentifiable;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.security.model.NoSuchAgentException;
import net.coalevo.userdata.model.Userdata;
import net.coalevo.userdata.service.UserdataXMLService;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * This is a cache for PUBLIC userdata instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class UserdataInstanceCache
    extends BaseIdentifiable {

  private Marker m_LogMarker = MarkerFactory.getMarker(UserdataInstanceCache.class.getName());
  private UserdataStore m_Store;
  private LRUCacheMap<String,UserdataImpl> m_Cache;
  private UserdataXMLService m_XMLService;

  public UserdataInstanceCache(UserdataStore store, UserdataXMLService xmls) {
    this(store, 50, xmls);
  }//private constructor

  public UserdataInstanceCache(UserdataStore store, int size, UserdataXMLService xmls) {
    super(UserdataInstanceCache.class.getName());
    m_Store = store;
    m_Cache = new LRUCacheMap<String,UserdataImpl>(size);
    m_XMLService = xmls;
  }//private constructor

  /**
   * Returns the maximum number of cached instances.
   *
   * @return the maximum number of cached instances.
   */
  public int getCacheSize() {
    return m_Cache.getCeiling();
  }//getCacheSize

  /**
   * Sets maximum number of cached instances.
   *
   * @param size the maximum cached instances.
   */
  public void setCacheSize(int size) {
    synchronized (m_Cache) {
      m_Cache.setCeiling(size);
    }
  }//setCacheSize

  private UserdataImpl createInstance(String str)
      throws NoSuchAgentException {
    ResultSet rs = null;
    Map<String,String> params = new HashMap<String,String>();
    params.put(AGENT_ID, str);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getUserdata", params);
      if (rs.next()) {
        //unmarshall from clob
        return ((EditableUserdataImpl) m_XMLService.fromXML(
            rs.getClob(1).getCharacterStream()
        )).toPublicUserdata();
      } else {
        throw new NoSuchAgentException(str);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"createInstance()", ex);
      return null;
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//createInstance

  public boolean contains(String id) {
    return m_Cache.containsKey(id);
  }//containsAgent

  public UserdataImpl get(String id)
      throws NoSuchAgentException {
    Object o = null;
    synchronized (m_Cache) {
      o = m_Cache.get(id);
    }
    if (o != null) {
      return (UserdataImpl) o;
    } else {
      UserdataImpl r = createInstance(id);
      synchronized (m_Cache) {
        m_Cache.put(id, r);
      }
      return r;
    }
  }//getAgent

  public void invalidate(Userdata a) {
    synchronized (m_Cache) {
      m_Cache.remove(a.getIdentifier());
    }
  }//invalidate

  public void invalidate(String id) {
    synchronized (m_Cache) {
      m_Cache.remove(id);
    }
  }//invalidate

  public void clear() {
    synchronized (m_Cache) {
      m_Cache.clear(false);
    }
  }//clear

  public String toString() {
    return m_Cache.toString();
  }//toString

  private static final String AGENT_ID = "agent_id";


}//class UserdataInstanceCache
