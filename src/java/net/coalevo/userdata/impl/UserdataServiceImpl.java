/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.security.model.*;
import net.coalevo.system.service.MailService;
import net.coalevo.system.service.MailServiceException;
import net.coalevo.userdata.model.EditableUserdata;
import net.coalevo.userdata.model.Userdata;
import net.coalevo.userdata.model.UserdataServiceException;
import net.coalevo.userdata.service.UserdataConfiguration;
import net.coalevo.userdata.service.UserdataService;
import net.coalevo.userdata.service.UserdataXMLService;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;
import java.sql.ResultSet;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Provides a RDBMS based implementation of a
 * {@link UserdataService} that includes a Lucene
 * index for full-text searching.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class UserdataServiceImpl
    extends BaseService
    implements UserdataService, ConfigurationUpdateHandler {

  private Marker m_LogMarker = MarkerFactory.getMarker(UserdataServiceImpl.class.getName());
  private ServiceMediator m_Services;
  private Messages m_BundleMessages;
  private boolean m_Active;

  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  private UserdataXMLService m_UserdataXMLService;
  private UserdataStore m_DataStore;
  private UserdataIndex m_PrivateIndex;
  private UserdataIndex m_PublicIndex;

  private UserdataInstanceCache m_UserdataCache;
  private AgentIdentifierInstanceCache m_AgentIdCache;
  private Map<AgentIdentifier, String> m_NickCache;

  //Transaction Tracking
  private WeakHashMap<String, EditableUserdataImpl> m_CreateUserdataTracker;
  private WeakHashMap<String, EditableUserdataImpl> m_UpdateUserdataTracker;

  //Validation Patterns
  private Pattern m_NicknamePattern;
  private Pattern m_AIDPattern;

  public UserdataServiceImpl(UserdataStore store) {
    super(UserdataService.class.getName(), ACTIONS);
    m_DataStore = store;
  }//constructor

  public boolean activate(BundleContext bc) {
    m_Services = Activator.getServices();
    m_BundleMessages = Activator.getBundleMessages();
    m_UserdataXMLService = m_Services.getUserdataXMLService();
    //1. Check if active
    if (m_Active) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.activate.active", "service", "UserdataService"));
      return false;
    }

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/UserdataService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);



    // Caches
    m_UserdataCache = m_DataStore.getUserdataCache();
    m_AgentIdCache = new AgentIdentifierInstanceCache();
    m_NickCache = m_DataStore.getNicknameCache();

    final String pipath = new File(Activator.getUserdataIndexPath(), "private").getAbsolutePath();
    final String pupath = new File(Activator.getUserdataIndexPath(), "public").getAbsolutePath();
    UserdataTransformer t = new UserdataTransformer();
    m_PrivateIndex = new UserdataIndex(pipath, t, false, m_AgentIdCache);
    m_PublicIndex = new UserdataIndex(pupath, t, true, m_AgentIdCache);
    try {
      m_PrivateIndex.init();
      if (!m_PrivateIndex.exists()) {
        m_PrivateIndex.rebuildIndex(m_DataStore);
        Activator.log().info(m_LogMarker, m_BundleMessages.get("UserdataServiceImpl.index.built", "index", "private"));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("UserdataServiceImpl.index.error", "index", "private"), ex);
    }
    Activator.log().info(m_LogMarker, m_BundleMessages.get("UserdataServiceImpl.index.success", "index", "private"));
    try {
      m_PublicIndex.init();
      if (!m_PublicIndex.exists()) {
        m_PublicIndex.rebuildIndex(m_DataStore);
        Activator.log().info(m_LogMarker, m_BundleMessages.get("UserdataServiceImpl.index.built", "index", "public"));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("UserdataServiceImpl.index.error", "index", "public"), ex);
    }
    Activator.log().info(m_LogMarker, m_BundleMessages.get("UserdataServiceImpl.index.success", "index", "public"));

    m_CreateUserdataTracker = new WeakHashMap<String, EditableUserdataImpl>(25);
    m_UpdateUserdataTracker = new WeakHashMap<String, EditableUserdataImpl>(25);

    //Prepare Patterns
    ConfigurationMediator cm = Activator.getServices().getConfigMediator();
    update(cm.getConfiguration());
    cm.addUpdateHandler(this);

    m_Active = true;
    return true;
  }//activate

  public boolean deactivate() {
    if (m_AgentIdCache != null) {
      m_AgentIdCache.clear();
      m_AgentIdCache = null;
    }
    if (m_UserdataCache != null) {
      m_UserdataCache.clear();
      m_UserdataCache = null;
    }
      if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }
    m_BundleMessages = null;
    m_Active = false;
    m_PrivateIndex = null;
    m_PublicIndex = null;
    return true;
  }//deactivate

  public void update(MetaTypeDictionary mtd) {
    String nickpattern = "[A-Z][a-zA-Z\\\\d ]{2,}+";
    String aidpattern = "[a-zA-Z][a-zA-Z\\\\d _]{2,}+";

    try {
      nickpattern = mtd.getString(UserdataConfiguration.VALIDATION_NICK_PATTERN_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataServiceImpl.config.exception", "attribute", UserdataConfiguration.VALIDATION_NICK_PATTERN_KEY),
          ex
      );
    }
    try {
      aidpattern = mtd.getString(UserdataConfiguration.VALIDATION_AID_PATTERN_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataServiceImpl.config.exception", "attribute", UserdataConfiguration.VALIDATION_AID_PATTERN_KEY),
          ex
      );
    }
    try {
      m_NicknamePattern = Pattern.compile(nickpattern);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataServiceImpl.config.exception", "attribute", UserdataConfiguration.VALIDATION_NICK_PATTERN_KEY),
          ex
      );
      //fallback
      m_NicknamePattern = Pattern.compile("[A-Z][a-z|A-Z|0-9| ]{2,}+");
    }
    try {
      m_AIDPattern = Pattern.compile(aidpattern);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataServiceImpl.config.exception", "attribute", UserdataConfiguration.VALIDATION_AID_PATTERN_KEY),
          ex
      );
      //fallback
      m_AIDPattern = Pattern.compile("[a-z|A-Z][a-z|A-Z|0-9|_]{2,}+");
    }
  }//update

  public EditableUserdata beginUserdataCreate(Agent caller, AgentIdentifier aid)
      throws SecurityException, UserdataServiceException {
    if (m_UserdataCache.contains(aid.getIdentifier())
        || existsUserdataEntry(aid)) {
      throw new UserdataServiceException(aid.getIdentifier());
    }
    //1. Check security
    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_USERDATA);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_USERDATA.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    EditableUserdataImpl eud = new EditableUserdataImpl(aid.getIdentifier());
    m_CreateUserdataTracker.put(eud.getIdentifier(), eud);
    return eud;
  }//beginUserdataCreate

  public EditableUserdata beginUserdataUpdate(Agent caller, AgentIdentifier aid)
      throws SecurityException, UserdataServiceException {

    //1. Check security
    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_USERDATA);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_USERDATA.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }

    //2. Load from database
    ResultSet rs = null;
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getIdentifier());
    LibraryConnection lc = null;
    EditableUserdataImpl eud = null;
    try {
      lc = m_DataStore.leaseConnection();
      rs = lc.executeQuery("getUserdata", params);
      if (rs.next()) {
        //unmarshall from clob
        eud = (EditableUserdataImpl) m_UserdataXMLService.fromXML(
            rs.getClob(1).getCharacterStream()
        );
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "beginUserdataUpdate()", ex);
      throw new UserdataServiceException("beginUserdataUpdate()", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
    }
    if (eud == null) {
      //3. Nosuch Agent if null
      throw new NoSuchAgentException(aid.getIdentifier());
    } else {
      //3. Add to tracker
      m_UpdateUserdataTracker.put(eud.getIdentifier(), eud);
      return eud;
    }
  }//beginUserdataUpdate

  public void cancelUserdataTransaction(Agent caller, EditableUserdata eud)
      throws SecurityException, IllegalStateException {

    EditableUserdataImpl eudi = (EditableUserdataImpl) eud;

    if (!((m_UpdateUserdataTracker.remove(eudi.getIdentifier()) != null)
        || (m_CreateUserdataTracker.remove(eudi.getIdentifier()) != null))) {
      throw new IllegalStateException();
    }
  }//cancelUserdataTransaction

  public void commitUserdata(Agent caller, EditableUserdata eud)
      throws SecurityException, IllegalStateException, UserdataServiceException {

    //Ensure that this is a tracked instance.
    EditableUserdataImpl eudi =
        m_UpdateUserdataTracker.get(eud.getIdentifier());
    String stmt = null;
    //1. handle update
    if (eudi != null) {
      stmt = "updateUserdata";
      m_UpdateUserdataTracker.remove(eud.getIdentifier());
    } else {
      //2. handle create
      eudi = m_CreateUserdataTracker.get(eud.getIdentifier());
      if (eudi != null) {
        stmt = "createUserdata";
        m_CreateUserdataTracker.remove(eud.getIdentifier());
      } else {
        throw new IllegalStateException();
      }
    }
    //3. Check security
    AgentIdentifier aid = m_AgentIdCache.get(eudi.getIdentifier());

    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_USERDATA);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_USERDATA.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    //4. Commit update
    doCommit(stmt, eudi, aid);
    //5. Email Update request async the confirmation
    if (stmt.equals("createUserdata") || eudi.isConfirmationNeeded()) {
      requestConfirmation(aid);
    }
    //6. Review needed
    if (eudi.isReviewNeeded()) {
      requestReview(aid);
    }
  }//commitUserdata

  private void doCommit(String stmt, EditableUserdataImpl eudi, AgentIdentifier aid)
      throws UserdataServiceException {
    //2. Generate XML
    String xml = null;
    try {
      xml = m_UserdataXMLService.toXML(eudi, false);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "doCommit()", ex);
      throw new UserdataServiceException();
    }

    //3. Store in database
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", eudi.getIdentifier());
    params.put("nickname", eudi.getNickname());
    params.put("email", eudi.getEmail());
    params.put("confirmed", (eudi.isConfirmed()) ? "1" : "0");
    params.put("reviewed", (eudi.isReviewed()) ? "1" : "0");
    params.put("document", xml);

    LibraryConnection lc = null;
    try {
      lc = m_DataStore.leaseConnection();
      lc.executeUpdate(stmt, params);
      m_UserdataCache.invalidate(eudi.getIdentifier());
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "doCommit()", ex);
    } finally {
      m_DataStore.releaseConnection(lc);
    }
    //if updated remove nick to be sure it didn't change
    m_NickCache.remove(aid);
  }//doCommit

  private void requestConfirmation(AgentIdentifier aid) {
    //1. Generate an identifier
    final String cuid =
        m_Services.getRndStringGeneratorService(ServiceMediator.NO_WAIT).getRandomString(10);

    //2. Update identifier in the database
    Map<String, String> params = new HashMap<String, String>();

    params.put("agent_id", aid.getIdentifier());
    LibraryConnection lc = null;
    try {
      lc = m_DataStore.leaseConnection();
      //a) delete existing id
      lc.executeUpdate("removeConfirmationID", params);
      //b) add new id
      params.put("confirmation_id", cuid);
      params.put("issued", Long.toString(System.currentTimeMillis()));
      lc.executeUpdate("addConfirmationID", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "requestConfirmation()::", ex);
    } finally {
      m_DataStore.releaseConnection(lc);
    }
    //3. Send mail
    //TODO: Handle Locale by userdata language?
    //TODO: Handle Formatting (probably through text service)?
    //Locale l = null;
    String email = getEmail(m_ServiceAgentProxy.getAuthenticPeer(), aid);
    String nickname = getNickname(m_ServiceAgentProxy.getAuthenticPeer(), aid);
    try {
      MessageAttributes attr = m_BundleMessages.leaseAttributes();
      attr.add("aid", aid.getIdentifier());
      attr.add("nick", nickname);
      attr.add("cuid", cuid);
      attr.add("hours", Integer.toString(m_DataStore.getConfirmationExpiry()));
      try {
        attr.add("wsbaseurl", m_Services.getConfigMediator().getConfiguration().getString(
            UserdataConfiguration.CONFIRM_WSBASEURLKEY_KEY));
        attr.add("policyurl", m_Services.getConfigMediator().getConfiguration().getString(
            UserdataConfiguration.CONFIRM_POLICYURL_KEY));
      } catch (MetaTypeDictionaryException e) {
        Activator.log().error("()", e);
      }
      MailService ms = m_Services.getMailService(ServiceMediator.NO_WAIT);
      if (ms == null) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.reference", "service", "MailService"));
        return;
      }
      try {
        ms.send(
            m_ServiceAgentProxy.getAuthenticPeer(),
            email,
            nickname,
            m_BundleMessages.get("Mail.confirmation.subject", "sysname",
                m_Services.getConfigMediator().getConfiguration().getString(
                    UserdataConfiguration.CONFIRM_SYSNAME_KEY)),
            m_BundleMessages.get("Mail.confirmation.body", attr)
        );
      } catch (MetaTypeDictionaryException e) {
        Activator.log().error("()", e);
      }
      Activator.log().info(m_LogMarker, m_BundleMessages.get("UserdataServiceImpl.mailconfirmation", "aid", aid.getIdentifier(), "email", email));
    } catch (MailServiceException ex) {
      Activator.log().error(m_LogMarker, "requestConfirmation()", ex);
    }
  }//requestConfirmation

  private void requestReview(AgentIdentifier aid) {

  }//requestReview

  public boolean isUserdataAvailable(Agent caller, AgentIdentifier aid) {
    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), IS_USERDATA_AVAILABLE);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", IS_USERDATA_AVAILABLE.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    //1. check cache
    //System.out.println("Check " + m_UserdataCache.contains(aid.getIdentifier()) + "::"  +  existsUserdataEntry(aid));
    return m_UserdataCache.contains(aid.getIdentifier()) || existsUserdataEntry(aid);
  }//isUserdataAvailable

  public Userdata getUserdata(Agent caller, AgentIdentifier aid)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_USERDATA);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_USERDATA.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    if (caller.getAgentIdentifier().equals(aid) ||
        m_PolicyProxy.checkAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_PRIVATE_USERDATA)) {
      ResultSet rs = null;
      Map<String, String> params = new HashMap<String, String>();
      params.put("agent_id", aid.getIdentifier());
      LibraryConnection lc = null;
      try {
        lc = m_DataStore.leaseConnection();
        rs = lc.executeQuery("getUserdata", params);
        if (rs.next()) {
          //unmarshall from clob
          return ((EditableUserdataImpl) m_UserdataXMLService.fromXML(
              rs.getClob(1).getCharacterStream()
          )).toUserdata();
        } 
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "getUserdata(Agent,AgentIdentifier)", ex);
      } finally {
        SqlUtils.close(rs);
        m_DataStore.releaseConnection(lc);
      }
      throw new NoSuchAgentException(aid.getIdentifier());
    } else {
      //1. magic cache :) (PUBLIC ONLY)
      return m_UserdataCache.get(aid.getIdentifier());
    }
  }//getUserdata

  public Userdata[] getUserdata(Agent caller, String nickname)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_USERDATA);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_USERDATA.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    ArrayList<Userdata> al = new ArrayList<Userdata>(5);
    Map<String, String> params = new HashMap<String, String>();
    params.put("nickname", nickname);
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_DataStore.leaseConnection();
      rs = lc.executeQuery("getUserdataByNickname", params);
      while (rs.next()) {
        UserdataImpl ud = ((EditableUserdataImpl) m_UserdataXMLService.fromXML(rs.getClob(1).getCharacterStream())
        ).toPublicUserdata();
        al.add(ud);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getUserdata(Agent,String)", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
    }
    return al.toArray(new Userdata[al.size()]);
  }//getUserdata

  public List<AgentIdentifier> listUnreviewedUserdata(Agent caller) {
    ArrayList<AgentIdentifier> al = new ArrayList<AgentIdentifier>(50);
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_DataStore.leaseConnection();
      rs = lc.executeQuery("listUnreviewedUserdata", null);
      while (rs.next()) {
        AgentIdentifier aid = m_AgentIdCache.get(rs.getString(1));
        al.add(aid);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listUnreviewedUserdata()", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
    }
    return al;
  }//listUnreviewedUserdata

  public List<AgentIdentifier> listUnconfirmedUserdata(Agent caller) {
    ArrayList<AgentIdentifier> al = new ArrayList<AgentIdentifier>(50);
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_DataStore.leaseConnection();
      rs = lc.executeQuery("listUnconfirmedUserdata", null);
      while (rs.next()) {
        AgentIdentifier aid = m_AgentIdCache.get(rs.getString(1));
        al.add(aid);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listUnconfirmedUserdata()", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
    }
    return al;
  }//listUnconfirmedUserdata

  public void removeUserdata(Agent caller, AgentIdentifier aid) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), DELETE_USERDATA);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_BundleMessages.get("error.action", "action", DELETE_USERDATA.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getIdentifier());

    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_DataStore.leaseConnection();
      lc.executeUpdate("deleteUserdata", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "removeUserdata()", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
      m_UserdataCache.invalidate(aid.getIdentifier());
    }
  }//deleteUserdata

  public boolean confirmEmail(Agent caller, AgentIdentifier aid, String confuid)
      throws SecurityException, UserdataServiceException {

    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CONFIRM_EMAIL);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CONFIRM_EMAIL.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    //2. Load from database
    ResultSet rs = null;
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getIdentifier());
    LibraryConnection lc = null;
    EditableUserdataImpl eud = null;
    try {
      lc = m_DataStore.leaseConnection();
      rs = lc.executeQuery("getConfirmationID", params);
      if (rs.next()) {
        String cfid = rs.getString(1);
        if (!cfid.equals(confuid)) {
          return false;
        } else {
          //3. Remove from database
          params.put("confirmation_id", cfid);
          lc.executeUpdate("removeConfirmationID", params);
          params.remove("confirmation_id");
        }
      } else {
        throw new UserdataServiceException(aid.getIdentifier());
      }
      rs = lc.executeQuery("getUserdata", params);
      if (rs.next()) {
        //unmarshall from clob
        eud = (EditableUserdataImpl) m_UserdataXMLService.fromXML(
            rs.getClob(1).getCharacterStream()
        );
      } else {
        throw new NoSuchAgentException(aid.getIdentifier());
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "confirmEmail()", ex);
      throw new UserdataServiceException("confirmEmail()", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
    }
    eud.setConfirmed(true);
    doCommit("updateUserdata", eud, aid);
    return true;
  }//confirmEmail

  public void retryConfirmation(Agent caller, AgentIdentifier aid)
      throws SecurityException {
    //1. Check security
    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), RESEND_CONFIRMATION);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", RESEND_CONFIRMATION.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    requestConfirmation(aid);
  }//retryConfirmation

  public void markReviewed(Agent caller, AgentIdentifier aid)
      throws SecurityException, UserdataServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), MARK_REVIEWED);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", MARK_REVIEWED.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //2. Load from database
    ResultSet rs = null;
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getIdentifier());
    LibraryConnection lc = null;
    EditableUserdataImpl eud = null;
    try {
      lc = m_DataStore.leaseConnection();
      rs = lc.executeQuery("getUserdata", params);
      if (rs.next()) {
        //unmarshall from clob
        eud = (EditableUserdataImpl) m_UserdataXMLService.fromXML(
            rs.getClob(1).getCharacterStream()
        );
      } else {
        throw new NoSuchAgentException(aid.getIdentifier());
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "markReviewed()::", ex);
      throw new UserdataServiceException("markReviewed()", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
    }
    eud.setReviewed(true);
    doCommit("updateUserdata", eud, aid);
  }//markReviewed

  public String getNickname(Agent caller, AgentIdentifier aid) {
    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_NICKNAME);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_NICKNAME.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    String nick = m_NickCache.get(aid);
    if (nick == null) {

      Map<String, String> params = new HashMap<String, String>();
      params.put("agent_id", aid.getIdentifier());
      ResultSet rs = null;
      LibraryConnection lc = null;
      try {
        lc = m_DataStore.leaseConnection();
        rs = lc.executeQuery("getNickname", params);
        if (rs.next()) {
          nick = rs.getString(1);
          m_NickCache.put(aid, nick);
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "getNickname()", ex);
      } finally {
        SqlUtils.close(rs);
        m_DataStore.releaseConnection(lc);
      }
    }
    if (nick == null) {
      throw new NoSuchAgentException(aid.getIdentifier());
    }
    return nick;
  }//getNickname

  public String getEmail(Agent caller, AgentIdentifier aid) {
    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_EMAIL);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_EMAIL.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getIdentifier());
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_DataStore.leaseConnection();
      rs = lc.executeQuery("getEmail", params);
      if (rs.next()) {
        return rs.getString(1);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getEmail()", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
    }
    throw new NoSuchAgentException(aid.getIdentifier());
  }//getEmail

  public AgentIdentifier[] getIdentifiers(Agent caller, String nickname)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_IDENTIFIERS);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_IDENTIFIERS.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    ArrayList<AgentIdentifier> al = new ArrayList<AgentIdentifier>(5);
    Map<String, String> params = new HashMap<String, String>();
    params.put("nickname", nickname);
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_DataStore.leaseConnection();
      rs = lc.executeQuery("getIdentifiersByNickname", params);
      while (rs.next()) {
        al.add(m_AgentIdCache.get(rs.getString(1)));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "existsNickname()", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
    }
    return al.toArray(new AgentIdentifier[al.size()]);
  }//getIdentifiers

  public boolean existsNickname(Agent caller, String nickname) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), EXISTS_NICKNAME);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", EXISTS_NICKNAME.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    Map<String, String> params = new HashMap<String, String>();
    params.put("nickname", nickname);
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_DataStore.leaseConnection();
      rs = lc.executeQuery("existsNickname", params);
      if (rs.next()) {
        return rs.getInt(1) > 0;
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "existsNickname()", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
    }
    return false;
  }//existsNickname

  public boolean isValidNickname(Agent caller, String nickname) throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), IS_VALID_NICKNAME);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", IS_VALID_NICKNAME.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_NicknamePattern.matcher(nickname).matches();
  }//isValidNickname

  public boolean isValidAgentIdentifier(Agent caller, String aid) throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), IS_VALID_AID);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", IS_VALID_AID.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_AIDPattern.matcher(aid).matches();
  }//isValidAgentIdentifier

  public boolean existsEmail(Agent caller, String email) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), EXISTS_EMAIL);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", EXISTS_EMAIL.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    Map<String, String> params = new HashMap<String, String>();
    params.put("email", email);
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_DataStore.leaseConnection();
      rs = lc.executeQuery("existsEmail", params);
      if (rs.next()) {
        return rs.getInt(1) > 0;
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "existsEmail()", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
    }
    return false;
  }//existsEmail

  private boolean existsUserdataEntry(AgentIdentifier aid) {
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getIdentifier());
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_DataStore.leaseConnection();
      rs = lc.executeQuery("existsUserdata", params);
      return rs.next() && (rs.getInt(1) > 0);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "isUserdataAvailable()", ex);
    } finally {
      SqlUtils.close(rs);
      m_DataStore.releaseConnection(lc);
    }
    return false;
  }//existsUserdataEntry

  public AgentIdentifier[] searchUserdata(Agent caller, String query) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_USERDATA);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_USERDATA.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    if (m_PolicyProxy.checkAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_PRIVATE_USERDATA)) {
      //Activator.log().debug("Searching private index.");
      return m_PrivateIndex.searchUserdata(query);
    } else {
      //Activator.log().debug("Searching public index.");
      return m_PublicIndex.searchUserdata(query);
    }
  }//searchUserdata

  public AgentIdentifier[] searchPublicUserdata(Agent caller, String query) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_USERDATA);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_USERDATA.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_PublicIndex.searchUserdata(query);
  }//searchPublicUserdata

  public void rebuildIndices(Agent caller)
      throws UserdataServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REBUILD_INDICES);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", REBUILD_INDICES.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    try {
      m_PrivateIndex.rebuildIndex(m_DataStore);
      Activator.log().info(m_LogMarker, m_BundleMessages.get("UserdataServiceImpl.index.built", "index", "private"));
      m_PublicIndex.rebuildIndex(m_DataStore);
      Activator.log().info(m_LogMarker, m_BundleMessages.get("UserdataServiceImpl.index.built", "index", "public"));
    } catch (Exception ex) {
      throw new UserdataServiceException();
    }
  }//rebuildIndices

  private static Action IS_USERDATA_AVAILABLE = new Action("isUserdataAvailable");
  private static Action GET_USERDATA = new Action("getUserdata");
  private static Action GET_PRIVATE_USERDATA = new Action("getPrivateUserdata");
  private static Action UPDATE_USERDATA = new Action("updateUserdata");
  private static Action CREATE_USERDATA = new Action("createUserdata");
  private static Action DELETE_USERDATA = new Action("deleteUserdata");
  private static Action GET_NICKNAME = new Action("getNickname");
  private static Action EXISTS_NICKNAME = new Action("existsNickname");
  private static Action GET_IDENTIFIERS = new Action("getIdentifiers");
  private static Action EXISTS_EMAIL = new Action("existsEmail");
  private static Action GET_EMAIL = new Action("getEmail");
  private static Action RESEND_CONFIRMATION = new Action("retryConfirmation");
  private static Action MARK_REVIEWED = new Action("markReviewed");
  private static Action CONFIRM_EMAIL = new Action("confirmEmail");
  private static Action REBUILD_INDICES = new Action("rebuildIndices");
  private static Action IS_VALID_NICKNAME = new Action("isValidNickname");
  private static Action IS_VALID_AID = new Action("isValidAgentIdentifier");


  private static Action[] ACTIONS = {
      IS_USERDATA_AVAILABLE, GET_USERDATA, GET_PRIVATE_USERDATA, UPDATE_USERDATA, CREATE_USERDATA,
      DELETE_USERDATA, GET_NICKNAME, EXISTS_NICKNAME, GET_IDENTIFIERS, RESEND_CONFIRMATION,
      MARK_REVIEWED, CONFIRM_EMAIL, GET_EMAIL, REBUILD_INDICES, IS_VALID_NICKNAME, IS_VALID_AID,
      Maintainable.DO_MAINTENANCE, Restoreable.DO_BACKUP, Restoreable.DO_RESTORE};

  public void doMaintenance(Agent caller) throws SecurityException, MaintenanceException {
    //check rights
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), Maintainable.DO_MAINTENANCE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", Maintainable.DO_MAINTENANCE.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    try {
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.start"));

      //maintain store
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.store"));
      m_DataStore.maintain(m_ServiceAgentProxy.getAuthenticPeer());

      //Update entry indices
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.entryindex", "index", "public"));
      try {
        m_PublicIndex.rebuildIndex(m_DataStore);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.entryindex", "index", "public"), ex);
      }

      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.entryindex", "index", "private"));
      try {
        m_PrivateIndex.rebuildIndex(m_DataStore);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.entryindex", "index", "private"), ex);
      }

      //clear trackers; this means that actually happening transactions will fail
      //however, it will remove any possibly existing stale transactions.
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.trackers"));
      m_CreateUserdataTracker.clear();
      m_UpdateUserdataTracker.clear();

      //clear local caches
      m_AgentIdCache.clear();

      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.end"));
    } catch (Exception ex) {
      throw new MaintenanceException(ex.getMessage(), ex);
    }
  }//doMaintenance

  public void doBackup(Agent caller, File f, String tag) throws SecurityException, BackupException {
    //TODO: Implement
  }//doBackup

  public void doRestore(Agent caller, File f, String tag) throws SecurityException, RestoreException {
    //TODO: Implement
  }//doRestore

}//class UserdataServiceImpl
