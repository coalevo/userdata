/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.userdata.model.Userdata;
import net.coalevo.userdata.model.UserdataTokens;
import net.coalevo.userdata.service.UserdataXMLService;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.AgentIdentifierInstanceCache;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.Hit;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.queryParser.QueryParser;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;

/**
 * Provides a persistent index for
 * searching {@link Userdata} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class UserdataIndex {

  private Marker m_LogMarker = MarkerFactory.getMarker(UserdataIndex.class.getName());
  private UserdataTransformer m_Transformer;
  private boolean m_Privacy;
  private String m_Path;

  private Directory m_Directory;
  private Analyzer m_Analyzer;
  private QueryParser m_QueryParser;
  private AgentIdentifierInstanceCache m_AgentIdCache;
  private IndexSearcher m_IndexSearcher;

  /**
   * Constructs this <tt>UserdataIndex</tt>.
   *
   * @param path the path to the index store.
   * @param t    the transformer to be used to obtain Lucene <tt>Document</tt> instances.
   * @param priv true if the index respects the privacy, false otherwise.
   * @param aidc a cache for agent identifiers.
   */
  public UserdataIndex(String path, UserdataTransformer t, boolean priv, AgentIdentifierInstanceCache aidc) {
    m_Privacy = priv;
    m_Transformer = t;
    m_Path = path;
    m_AgentIdCache = aidc;
  }//constructor

  /**
   * Initializes this <tt>UserdataIndex</tt>.
   *
   * @throws IOException if the I/O fails.
   */
  public void init() throws IOException {
    prepareAnalyzer();
    final File f = new File(m_Path);
    m_Directory = FSDirectory.getDirectory(m_Path, !f.exists());
    m_QueryParser = new QueryParser(UserdataTokens.ELEMENT_INFO, m_Analyzer);

  }//init

  public boolean exists() throws IOException {
    return IndexReader.indexExists(m_Directory);
  }//exists

  public synchronized void rebuildIndex(UserdataStore store) throws IOException {
    UserdataXMLService xmls = Activator.getServices().getUserdataXMLService();
    if (m_Directory == null) {
      init();
    }
    IndexWriter iw = new IndexWriter(m_Directory, m_Analyzer, true);
    //bulk import
    Userdata data = null;
     ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = store.leaseConnection();
      rs = lc.executeQuery("listUserdata",null);

      while(rs.next()) {
        try {
          data = (UserdataImpl) xmls.fromXML(rs.getClob(1).getCharacterStream());
          addUserdata(iw,data);
        } catch (IOException ioex) {
          Activator.log().error(m_LogMarker,"rebuildIndex()", ioex);
        }
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"rebuildIndex()", ex);
    } finally {
      SqlUtils.close(rs);
      store.releaseConnection(lc);
    }

    //optimize
    iw.optimize();
    iw.close();
    //invalidate searcher
    m_IndexSearcher = null;    
  }//rebuildIndex

  public AgentIdentifier[] searchUserdata(String query) {
    Set<AgentIdentifier> s = new HashSet<AgentIdentifier>();
    try {
      Query q = null;
      //TODO: probably pool, may not scale
      synchronized(m_QueryParser) {
        q = m_QueryParser.parse(query);
      }
      //Activator.log().debug("Query: " + q.toString());
      if(m_IndexSearcher == null) {
        m_IndexSearcher = getSearcher();
      }
      Hits hits = m_IndexSearcher.search(q);
      //Activator.log().debug("Hits: " + hits.length());

      for (Iterator iterator = hits.iterator(); iterator.hasNext();) {
        Hit h = (Hit) iterator.next();
        s.add(m_AgentIdCache.get(h.get(UserdataTokens.ATTR_USERDATA_UID)));
        //Activator.log().debug("Matched " + h.get(UserdataTokens.ATTR_USERDATA_UID));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"searchUserdata()",ex);
    }
    return s.toArray(new AgentIdentifier[s.size()]);
  }//searchUserdata

  /**
   * Adds a {@link Userdata} instance to this index, using
   * an already open writer.
   * </p>
   *
   * @param iw the IndexWriter to be used.
   * @param data the data to be added as Document.
   * @throws IOException if the I/O fails.
   */
  private void addUserdata(IndexWriter iw, Userdata data)
      throws IOException {
    iw.addDocument(m_Transformer.toDocument(data, m_Privacy));
  }//bulkAddUserdata

  /**
   * Returns an <tt>IndexReader</tt> for the wrapped index.
   *
   * @return an <tt>IndexReader</tt> instance.
   * @throws IOException if an I/O error occurs.
   */
  public IndexReader getReader() throws IOException {
    return IndexReader.open(m_Directory);
  }//getReader

  /**
   * Returns an <tt>IndexSearcher</tt> for the wrapped index.
   *
   * @return an <tt>IndexSearcher</tt> instance.
   * @throws IOException if an I/O error occurs.
   */
  public IndexSearcher getSearcher() throws IOException {
    return new IndexSearcher(m_Directory);
  }//getSearcher

  /**
   * Prepares the analyzer.
   */
  private void prepareAnalyzer() {
    m_Analyzer = new SimpleAnalyzer();
  }//prepareAnalyzer

}//class UserdataIndex
