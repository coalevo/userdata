/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.userdata.model.Userdata;
import net.coalevo.userdata.model.UserdataPrivacy;
import net.coalevo.userdata.model.UserdataTokens;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.DateTools;

import java.io.IOException;
import java.util.Iterator;

/**
 * Provides methods for transforming userdata instances into
 * Lucene <tt>Document</tt> instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class UserdataTransformer {


  public UserdataTransformer() {
  }//constructor

  /**
   * Private index add, indexes all, stores all names, email, flags
   *
   * @param data    the {@link Userdata} instance to be transformed.
   * @param privacy true if the privacy should be observed, false otherwise.
   * @return a Document for a Lucene index.
   * @throws IOException if the I/O fails.
   */
  public Document toDocument(Userdata data, boolean privacy) throws IOException {
    Document udd = new Document();
    //construct an instance that allows access to all data
    UserdataPrivacy priv = UserdataPrivacyImpl.PUBLIC_ALL;
    if (privacy) {
      priv = data.getPrivacy();
    }

    addField(udd, UserdataTokens.ATTR_USERDATA_UID, data.getIdentifier(),true,false);
    addField(udd, UserdataTokens.ELEMENT_NICKNAME, data.getNickname(),true,true);

    String tmp = data.getPrefix();
    if (priv.getAllowsPrefix() && tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_PREFIX, tmp,false,true);
    }

    tmp = data.getFirstnames();
    if (priv.getAllowsFirstnames()&& tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_FIRSTNAME, tmp,false,true);
    }

    tmp = data.getAdditionalnames();
    if (priv.getAllowsAdditionalnames()&& tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_ADDITIONALNAME, tmp,false,true);
    }

    tmp=data.getLastnames();
    if (priv.getAllowsLastnames()&& tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_LASTNAME, tmp,false,true);
    }

    tmp=data.getSuffix();
    if (priv.getAllowsSuffix()&& tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_SUFFIX, tmp,false,true);
    }

    if (priv.getAllowsBirthdate() && data.getBirthdate() != null) {
      addField(udd, UserdataTokens.ELEMENT_BIRTHDATE,
          DateTools.dateToString(data.getBirthdate(),DateTools.Resolution.DAY),false,false);
    }

    tmp = data.getExtension();
    if (priv.getAllowsExtension()&& tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_EXTENSION, tmp,false,true);
    }

    tmp= data.getCity();
    if (priv.getAllowsCity()&& tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_CITY, tmp,false,true);
    }

    tmp = data.getCountry();
    if (priv.getAllowsCountry() && tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_COUNTRY, tmp,false,true);
    }

    tmp=data.getPostalCode();
    if (priv.getAllowsPostalCode()&& tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_POSTALCODE,tmp,false,false);
    }

    tmp = data.getEmail();
    if (priv.getAllowsEmail()&& tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_EMAIL,tmp,false,true);
    }

    tmp = data.getLink();
    if(tmp != null && tmp.length() > 0) {
    addField(udd, UserdataTokens.ELEMENT_LINK,tmp,false,true);
    }

    tmp = data.getLinkType();
    if(tmp != null && tmp.length() > 0) {
      addField(udd, "linktype", tmp,false,false);
    }

    tmp = data.getPhone();
    if (priv.getAllowsPhone()&& tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_PHONE, tmp,false,true);
    }

    tmp=data.getMobile();
    if (priv.getAllowsMobile() && tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_MOBILE, tmp,false,true);
    }

    tmp = data.getLanguages();
    if(tmp != null && tmp.length() > 0){
      addField(udd, UserdataTokens.ELEMENT_LANGUAGES, tmp,false,true);
    }

    addField(udd, "photo", (data.hasPhoto()) ? "true" : "false",false,false);

    tmp = data.getInfo();
    if(tmp != null && tmp.length() > 0) {
      addField(udd, UserdataTokens.ELEMENT_INFO, tmp,false, true);
    }
    tmp = data.getInfoFormat();
    if(tmp != null && tmp.length() > 0) {
      addField(udd,UserdataTokens.ATTR_FORMAT,tmp,false,false);
    }
    
    //flags
    Iterator iter = data.getFlags();
    if (iter.hasNext()) {
      for (Iterator iterator = data.getFlags(); iterator.hasNext();) {
        tmp = (String) iterator.next();
        addField(udd, UserdataTokens.ELEMENT_FLAG, tmp,false,true);
      }
    }

    if (data.getFirstLoginDate() != null) {
      addField(udd, UserdataTokens.ELEMENT_FIRSTLOGINDATE,
          DateTools.dateToString(data.getFirstLoginDate(),DateTools.Resolution.DAY)
          ,false,false);
    }
    //System.err.println("Transformed: " + udd.toString());
    return udd;
  }//toDocument

  private void addField(Document udd, String name, String txt, boolean store, boolean tokenize) {
    if (txt != null) {
      Field f = new Field(
          name,
          txt,
          (store) ? Field.Store.YES : Field.Store.NO,
          (tokenize) ? Field.Index.TOKENIZED : Field.Index.UN_TOKENIZED
      );
      udd.add(f);
    }
  }//addField


}//class UserdataTransformer
