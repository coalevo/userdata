/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.foundation.model.BaseService;
import net.coalevo.stax.service.StAXFactoryService;
import net.coalevo.userdata.model.EditableUserdataPrivacy;
import net.coalevo.userdata.model.Userdata;
import net.coalevo.userdata.model.UserdataPrivacy;
import net.coalevo.userdata.model.UserdataTokens;
import net.coalevo.userdata.service.UserdataXMLService;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.xml.stream.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * Provides an implementation of {@link UserdataXMLService}.
 * <p/>
 * XML output is created manually appending to a final
 * <tt>StringBuilder</tt> instance.<br/>
 * XML input is parsed through an optimized SAX
 * document handler that is implemented manually with
 * a Finite State Machine.
 * </p>
 * <p/>
 * Additionally stream based versions of parsers are provided.
 * This support was introduced for EMPP.
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class UserdataXMLServiceImpl
    extends BaseService
    implements UserdataXMLService {

  private Marker m_LogMarker = MarkerFactory.getMarker(UserdataXMLServiceImpl.class.getName());

  private XMLInputFactory m_XMLInputFactory;
  private XMLOutputFactory m_XMLOutputFactory;

  public UserdataXMLServiceImpl() {
    super(UserdataXMLServiceImpl.class.getName(), null);
  }//constructor

  public boolean activate(BundleContext bc) {

    try {
      StAXFactoryService stax = Activator.getServices().getStAXFactoryService(ServiceMediator.WAIT_UNLIMITED);
      m_XMLInputFactory = stax.getXMLInputFactory();
      m_XMLOutputFactory = stax.getXMLOutputFactory();
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
    }

    return true;
  }//activate

  public boolean deactivate() {
    m_XMLInputFactory = null;
    m_XMLOutputFactory = null;
    return true;
  }//deactivate

  public Userdata fromXML(String xml) throws IOException {

    final StringReader r = new StringReader(xml);
    try {
      return fromXML(forInput(r));
    } catch (XMLStreamException ex) {
      throw (IOException) new IOException().initCause(ex);
    }
  }//fromXML

  public Userdata fromXML(Reader r) throws IOException {
    try {
      return fromXML(forInput(r));
    } catch (XMLStreamException ex) {
      throw (IOException) new IOException().initCause(ex);
    }
  }//fromXML

  public Userdata fromXML(InputStream r) throws IOException {
    try {
      return fromXML(forInput(r));
    } catch (XMLStreamException ex) {
      throw (IOException) new IOException().initCause(ex);
    }
  }//fromXML

  public Userdata fromXML(XMLStreamReader reader)
      throws XMLStreamException {

    EditableUserdataImpl ud = new EditableUserdataImpl();
    EditableUserdataPrivacy priv = ud.getEditablePrivacy();
    boolean reviewed = false;
    boolean confirmed = false;

    if (!reader.isStartElement()) {
      reader.nextTag();
    }
    String name = reader.getLocalName();
    String nsuri = reader.getNamespaceURI();
    //System.err.println("Name=" + name + "::NS=" + nsuri);
    if (UserdataTokens.ELEMENT_USERDATA.equals(name) && UserdataTokens.NAMESPACE.equals(nsuri)) {
      //1. read attributes, v1.0 and identifier
      for (int i = 0; i < reader.getAttributeCount(); i++) {
        String attr = reader.getAttributeName(i).getLocalPart();
        String val = reader.getAttributeValue(i);
        //System.out.println("Attrname=" + attr + "::Val=" + val);
        if (UserdataTokens.ATTR_USERDATA_VERSION.equals(attr)) {
          if (!UserdataTokens.VERSION.equals(val)) {
            throw new XMLStreamException("Malformed stream.");
          }
        } else if (UserdataTokens.ATTR_USERDATA_UID.equals(attr)) {
          ud.setIdentifier(val);
        } else if (UserdataTokens.ATTR_CONFIRMED.equals(attr)) {
          confirmed = Boolean.parseBoolean(val);
        } else if (UserdataTokens.ATTR_REVIEWED.equals(attr)) {
          reviewed = Boolean.parseBoolean(val);
        }
      }
      //Nick
      reader.nextTag();
      name = reader.getLocalName();
      if (UserdataTokens.ELEMENT_NICKNAME.equals(name)) {
        ud.setNickname(reader.getElementText());
        reader.nextTag();
        name = reader.getLocalName();
      }

      //Personal
      if (UserdataTokens.ELEMENT_PERSONAL.equals(name)) {
        reader.nextTag();
        name = reader.getLocalName();
        if (UserdataTokens.ELEMENT_PREFIX.equals(name)) {
          priv.setAllowsPrefix(getAccess(reader));
          ud.setPrefix(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_FIRSTNAME.equals(name)) {
          priv.setAllowsFirstnames(getAccess(reader));
          ud.setFirstnames(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_ADDITIONALNAME.equals(name)) {
          priv.setAllowsAdditionalnames(getAccess(reader));
          ud.setAdditionalNames(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_LASTNAME.equals(name)) {
          priv.setAllowsLastnames(getAccess(reader));
          ud.setLastnames(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_SUFFIX.equals(name)) {
          priv.setAllowsSuffix(getAccess(reader));
          ud.setSuffix(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();

        }
        if (UserdataTokens.ELEMENT_BIRTHDATE.equals(name)) {
          priv.setAllowsBirthdate(getAccess(reader));
          try {
            ud.setBirthdate(DATE_FORMAT.parse(reader.getElementText()));
            reader.nextTag();
            name = reader.getLocalName();

          } catch (ParseException ex) {
            throw new XMLStreamException("Malformed stream (birthdate).");
          }
        }
        if (UserdataTokens.ELEMENT_GENDER.equals(name)) {
          priv.setAllowsGender(getAccess(reader));
          ud.setGender(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        reader.nextTag();
        name = reader.getLocalName();
      }

      //Address
      if (UserdataTokens.ELEMENT_ADDRESS.equals(name)) {
        reader.nextTag();
        name = reader.getLocalName();
        if (UserdataTokens.ELEMENT_STREET.equals(name)) {
          priv.setAllowsStreet(getAccess(reader));
          ud.setStreet(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_EXTENSION.equals(name)) {
          priv.setAllowsExtension(getAccess(reader));
          ud.setExtension(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_CITY.equals(name)) {
          priv.setAllowsCity(getAccess(reader));
          ud.setCity(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_COUNTRY.equals(name)) {
          priv.setAllowsCountry(getAccess(reader));
          ud.setCountry(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_POSTALCODE.equals(name)) {
          priv.setAllowsPostalCode(getAccess(reader));
          ud.setPostalCode(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();

        }
        reader.nextTag();
        name = reader.getLocalName();
      }

      //Communications
      if (UserdataTokens.ELEMENT_COMMUNICATIONS.equals(name)) {
        reader.nextTag();
        name = reader.getLocalName();
        if (UserdataTokens.ELEMENT_EMAIL.equals(name)) {
          priv.setAllowsEmail(getAccess(reader));
          ud.setEmail(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_LINK.equals(name)) {
          if (reader.getAttributeCount() > 0) {
            ud.setLinkType(reader.getAttributeValue(0));
          }
          ud.setLink(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_PHONE.equals(name)) {
          priv.setAllowsPhone(getAccess(reader));
          ud.setPhone(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_MOBILE.equals(name)) {
          priv.setAllowsMobile(getAccess(reader));
          ud.setMobile(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        if (UserdataTokens.ELEMENT_LANGUAGES.equals(name)) {
          ud.setLanguages(reader.getElementText());
          reader.nextTag();
          name = reader.getLocalName();
        }
        reader.nextTag();
        name = reader.getLocalName();
      }

      //Photo
      if (UserdataTokens.ELEMENT_PHOTO.equals(name)) {
        //should have just one
        if (reader.getAttributeCount() > 0) {
          ud.setPhotoType(reader.getAttributeValue(0));
        }
        ud.setPhoto(reader.getElementText());
        reader.nextTag();
        name = reader.getLocalName();
      }
      //info
      if (UserdataTokens.ELEMENT_INFO.equals(name)) {
        //should have just one
        if (reader.getAttributeCount() > 0) {
          ud.setInfoFormat(reader.getAttributeValue(0));
        }
        ud.setInfo(reader.getElementText());
        reader.nextTag();
        name = reader.getLocalName();
      }
      //flags
      if (UserdataTokens.ELEMENT_FLAGS.equals(name)) {
        boolean done = false;
        do {
          reader.nextTag();
          name = reader.getLocalName();
          if (UserdataTokens.ELEMENT_FLAG.equals(name)) {
            ud.addFlag(reader.getElementText());
          } else {
            done = true;
          }
        } while (!done);
        if (UserdataTokens.ELEMENT_FLAGS.equals(name)) {
          reader.nextTag();
          name = reader.getLocalName();
        }
      }
      //firstlogin
      if (UserdataTokens.ELEMENT_FIRSTLOGINDATE.equals(name)) {
        try {
          ud.setFirstLoginDate(DATE_FORMAT.parse(reader.getElementText()));
          reader.nextTag();
          name = reader.getLocalName();
        } catch (ParseException ex) {
          throw new XMLStreamException("Malformed stream (firstlogindate).");
        }
      }
    } else {
      throw new XMLStreamException("Malformed stream.");
    }
    ud.setReviewed(reviewed);
    ud.setConfirmed(confirmed);
    return ud;
  }//fromXML

  private boolean getAccess(XMLStreamReader reader) {
    return reader.getAttributeCount() <= 0 || !Boolean.valueOf(reader.getAttributeValue(0));
  }//getAccess

  private final XMLStreamReader forInput(InputStream input)
      throws XMLStreamException {
    return m_XMLInputFactory.createXMLStreamReader(input);
  }//forInput

  private final XMLStreamReader forInput(Reader input)
      throws XMLStreamException {
    return m_XMLInputFactory.createXMLStreamReader(input);
  }//forInput

  public String toXML(Userdata ud, boolean filter) throws IOException {

    final StringWriter w = new StringWriter();
    try {
      toXML(forOutput(w), ud, filter);
      return w.toString();
    } catch (XMLStreamException ex) {
      throw (IOException) new IOException().initCause(ex);
    }
  }//toXML

  public void toXML(Writer w, Userdata ud, boolean filter) throws IOException {

    try {
      toXML(forOutput(w), ud, filter);
    } catch (XMLStreamException ex) {
      throw (IOException) new IOException().initCause(ex);
    }
  }//toXML

  public void toXML(OutputStream w, Userdata ud, boolean filter) throws IOException {

    try {
      toXML(forOutput(w), ud, filter);
    } catch (XMLStreamException ex) {
      throw (IOException) new IOException().initCause(ex);
    }
  }//toXML

  public void toXML(XMLStreamWriter w, Userdata ud, boolean filter) throws XMLStreamException {
    UserdataPrivacy priv = UserdataPrivacyImpl.PRIVATE_ALL;
    if (ud.getPrivacy() != null) {
      priv = ud.getPrivacy();
    }

    w.writeStartElement(UserdataTokens.ELEMENT_USERDATA);
    w.writeNamespace("", UserdataTokens.NAMESPACE);
    w.writeAttribute(UserdataTokens.ATTR_USERDATA_VERSION, UserdataTokens.VERSION);
    w.writeAttribute(UserdataTokens.ATTR_USERDATA_UID, ud.getIdentifier());
    w.writeAttribute(UserdataTokens.ATTR_CONFIRMED, Boolean.toString(ud.isConfirmed()));
    w.writeAttribute(UserdataTokens.ATTR_REVIEWED, Boolean.toString(ud.isReviewed()));
    writeSimple(w, UserdataTokens.ELEMENT_NICKNAME, ud.getNickname());

    //write personal
    boolean start = false;
    String tmp = null;

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_PREFIX, ud.getPrefix(), priv.getAllowsPrefix());

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_FIRSTNAME, ud.getFirstnames(), priv.getAllowsFirstnames());

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_ADDITIONALNAME, ud.getAdditionalnames(), priv.getAllowsAdditionalnames());

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_LASTNAME, ud.getLastnames(), priv.getAllowsLastnames());

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_SUFFIX, ud.getSuffix(), priv.getAllowsSuffix());

    Date d = ud.getBirthdate();
    if (d != null) {
      tmp = DATE_FORMAT.format(ud.getBirthdate());
      start = writeComplexHideable(
          w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
          UserdataTokens.ELEMENT_BIRTHDATE, tmp, priv.getAllowsBirthdate());
    }
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_GENDER, ud.getGender(), priv.getAllowsGender());

    if (start) {
      w.writeEndElement();
    }

    //write address
    start = false;
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_ADDRESS, start,
        UserdataTokens.ELEMENT_STREET, ud.getStreet(), priv.getAllowsStreet());
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_ADDRESS, start,
        UserdataTokens.ELEMENT_EXTENSION, ud.getExtension(), priv.getAllowsExtension());
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_ADDRESS, start,
        UserdataTokens.ELEMENT_CITY, ud.getCity(), priv.getAllowsCity());
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_ADDRESS, start,
        UserdataTokens.ELEMENT_COUNTRY, ud.getCountry(), priv.getAllowsCountry());
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_ADDRESS, start,
        UserdataTokens.ELEMENT_POSTALCODE, ud.getPostalCode(), priv.getAllowsPostalCode());
    if (start) {
      w.writeEndElement();
    }

    //communications
    start = false;
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_COMMUNICATIONS, start,
        UserdataTokens.ELEMENT_EMAIL, ud.getEmail(), priv.getAllowsEmail());

    tmp = ud.getLink();
    if (tmp != null && tmp.length() > 0) {
      start = conditionalStart(start, w, UserdataTokens.ELEMENT_COMMUNICATIONS);
      writeAttributeElement(w, UserdataTokens.ELEMENT_LINK, UserdataTokens.ATTR_TYPE, ud.getLinkType(), ud.getLink());
    }
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_COMMUNICATIONS, start,
        UserdataTokens.ELEMENT_PHONE, ud.getPhone(), priv.getAllowsPhone());

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_COMMUNICATIONS, start,
        UserdataTokens.ELEMENT_MOBILE, ud.getMobile(), priv.getAllowsMobile());

    tmp = ud.getLanguages();
    if (tmp != null && tmp.length() > 0) {
      start = conditionalStart(start, w, UserdataTokens.ELEMENT_COMMUNICATIONS);
      writeSimple(w, UserdataTokens.ELEMENT_LANGUAGES, tmp);
    }
    if (start) {
      w.writeEndElement();
    }

    //photo
    tmp = ud.getPhoto();
    if (tmp != null && tmp.length() > 0) {
      writeAttributeElement(w, UserdataTokens.ELEMENT_PHOTO, UserdataTokens.ATTR_TYPE, ud.getPhotoType(), tmp);
    }
    //info
    tmp = ud.getInfo();
    if (tmp != null && tmp.length() > 0) {
      writeAttributeElement(w, UserdataTokens.ELEMENT_INFO, UserdataTokens.ATTR_FORMAT, ud.getInfoFormat(), tmp);
    }
    //flags
    Iterator iter = ud.getFlags();
    if (iter.hasNext()) {

      w.writeStartElement(UserdataTokens.ELEMENT_FLAGS);
      for (Iterator iterator = ud.getFlags(); iterator.hasNext();) {
        tmp = (String) iterator.next();
        writeSimple(w, UserdataTokens.ELEMENT_FLAG, tmp);
      }
      w.writeEndElement();
    }

    //firstlogin
    d = ud.getFirstLoginDate();
    if (d != null) {
      writeSimple(w, UserdataTokens.ELEMENT_FIRSTLOGINDATE, DATE_FORMAT.format(d));
    }

    tmp = ud.getAnnotations();
    if (tmp != null && tmp.length() > 0) {
      writeSimple(w, UserdataTokens.ELEMENT_ANNOTATIONS, tmp);
    }

    w.writeEndElement();
    w.flush();

  }//writeTo

  private void writeSimple(XMLStreamWriter writer, String element, String content)
      throws XMLStreamException {
    writer.writeStartElement(element);
    writer.writeCharacters(content);
    writer.writeEndElement();
  }//writeSimple

  private boolean writeComplexHideable(XMLStreamWriter writer, boolean filter, String pelem, boolean start, String element, String content, boolean allow)
      throws XMLStreamException {
    if (filter && !allow) {
      return start;
    }
    if (content != null && content.length() > 0) {
      start = conditionalStart(start, writer, pelem);
      writeSimpleHideableElement(writer, element, content, allow);
    }
    return start;
  }//writeComplexHideable

  private boolean conditionalStart(boolean b, XMLStreamWriter w, String element) throws XMLStreamException {
    if (!b) {
      w.writeStartElement(element);
      return true;
    } else {
      return b;
    }
  }//conditionalStart

  private void writeSimpleHideableElement(XMLStreamWriter w, String element, String value, boolean allow)
      throws XMLStreamException {
    w.writeStartElement(element);
    if (!allow) {
      w.writeAttribute(UserdataTokens.ATTR_HIDDEN, "true");
    }
    w.writeCharacters(value);
    w.writeEndElement();
  }//writeSimpleHideableElement

  private void writeAttributeElement(XMLStreamWriter w, String element, String attname, String attvalue, String value)
      throws XMLStreamException {
    w.writeStartElement(element);
    if (attvalue != null && attvalue.length() > 0) {
      w.writeAttribute(attname, attvalue);
    }
    w.writeCharacters(value);
    w.writeEndElement();
  }//writeAttributeElement

  private final XMLStreamWriter forOutput(Writer output)
      throws XMLStreamException {
    return m_XMLOutputFactory.createXMLStreamWriter(output);
  }//forOutput

  private final XMLStreamWriter forOutput(OutputStream output)
      throws XMLStreamException {
    return m_XMLOutputFactory.createXMLStreamWriter(output);
  }//forOutput

  private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

}//class UserdataXMLServiceImpl
