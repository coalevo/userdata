/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.datasource.service.DataSourceService;
import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.service.RndStringGeneratorService;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.stax.service.StAXFactoryService;
import net.coalevo.system.service.MailService;
import net.coalevo.userdata.service.UserdataXMLService;
import net.coalevo.security.service.SecurityManagementService;
import org.osgi.framework.*;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Provides a mediator for required coalevo services.
 * Allows to obtain fresh and latest references by using
 * the whiteboard model to track the services at all times.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceMediator {

  private static Marker m_LogMarker = MarkerFactory.getMarker(ServiceMediator.class.getName());
  private BundleContext m_BundleContext;
  private RndStringGeneratorService m_RndStringGeneratorService;
  private MessageResourceService m_MessageResourceService;
  private MailService m_MailService;
  private DataSourceService m_DataSourceService;
  private StAXFactoryService m_StAXFactoryService;

  private CountDownLatch m_RndStringGeneratorLatch;
  private CountDownLatch m_MessageResourceServiceLatch;
  private CountDownLatch m_MailServiceLatch;
  private CountDownLatch m_DataSourceServiceLatch;
  private CountDownLatch m_StAXFactoryServiceLatch;


  private ConfigurationMediator m_ConfigMediator;
  private UserdataXMLService m_UserdataXMLService;

  public ServiceMediator() {

  }//constructor

  public StAXFactoryService getStAXFactoryService(long wait) throws Exception {
    try {
      if (wait < 0) {
        m_StAXFactoryServiceLatch.await();
      } else if (wait > 0) {
        m_StAXFactoryServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getStAXFactoryService()", e);
    }
    return m_StAXFactoryService;
  }//getStAXFactoryService

  public MessageResourceService getMessageResourceService(long wait) {
    try {
      if (wait < 0) {
        m_MessageResourceServiceLatch.await();
      } else if (wait > 0) {
        m_MessageResourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getMessageResourceService()", e);
    }
    return m_MessageResourceService;
  }//getMessageResourceService

  public RndStringGeneratorService getRndStringGeneratorService(long wait) {
    try {
      if (wait < 0) {
        m_RndStringGeneratorLatch.await();
      } else if (wait > 0) {
        m_RndStringGeneratorLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getRndStringGeneratorService()", e);
    }
    return m_RndStringGeneratorService;
  }//getRndStringGeneratorService

  public MailService getMailService(long wait) {
    try {
      if (wait < 0) {
        m_MailServiceLatch.await();
      } else if (wait > 0) {
        m_MailServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getMailService()", e);
    }
    return m_MailService;
  }//getMailService

  public DataSourceService getDataSourceService(long wait) {
    try {
      if (wait < 0) {
        m_DataSourceServiceLatch.await();
      } else if (wait > 0) {
        m_DataSourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getDataSourceService()", e);
    }
    return m_DataSourceService;
  }//getDataSourceService

  public SecurityManagementService getSecurityManagementService() throws Exception{
    ServiceReference sr =
       m_BundleContext.getServiceReference(SecurityManagementService.class.getName());
    if (sr == null) {
      throw new Exception(SecurityManagementService.class.getName() + " not found.");
    }
    Object o = m_BundleContext.getService(sr);
    if (o != null) {
     return (SecurityManagementService) o;
    } else {
      throw new Exception(SecurityManagementService.class.getName() + " unavailable.");
    }
  }//getSecurityManagementService

  public UserdataXMLService getUserdataXMLService() {
    return m_UserdataXMLService;
  }//getUserdataXMLService

  public void setUserdataXMLService(UserdataXMLService userdataXMLService) {
    m_UserdataXMLService = userdataXMLService;
  }//setUserdataXMLService

  public ConfigurationMediator getConfigMediator() {
    return m_ConfigMediator;
  }//getConfigMediator

  public void setConfigMediator(ConfigurationMediator configMediator) {
    m_ConfigMediator = configMediator;
  }//setConfigMediator

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare waits if required
    m_RndStringGeneratorLatch = createWaitLatch();
    m_MessageResourceServiceLatch = createWaitLatch();
    m_MailServiceLatch = createWaitLatch();
    m_DataSourceServiceLatch = createWaitLatch();
    m_StAXFactoryServiceLatch = createWaitLatch();

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(|(|(|(|(objectclass=" + RndStringGeneratorService.class.getName() + ")" +
            "(objectclass=" + MessageResourceService.class.getName() + "))" +
            "(objectclass=" + MailService.class.getName() + "))" +
            "(objectclass=" + DataSourceService.class.getName() + "))" +
            "(objectclass=" + StAXFactoryService.class.getName() + "))";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    //null out the references
    m_RndStringGeneratorService = null;
    m_MessageResourceService = null;
    m_MailService = null;
    m_DataSourceService = null;
    m_StAXFactoryService = null;

    if(m_RndStringGeneratorLatch != null) {
      m_RndStringGeneratorLatch.countDown();
      m_RndStringGeneratorLatch = null;
    }
    if(m_MessageResourceServiceLatch != null) {
      m_MessageResourceServiceLatch.countDown();
      m_MessageResourceServiceLatch = null;
    }
    if(m_MailServiceLatch != null) {
      m_MailServiceLatch.countDown();
      m_MailServiceLatch = null;
    }
    if(m_DataSourceServiceLatch != null) {
      m_DataSourceServiceLatch.countDown();
      m_DataSourceServiceLatch = null;
    }
    if(m_StAXFactoryServiceLatch != null) {
      m_StAXFactoryServiceLatch.countDown();
      m_StAXFactoryServiceLatch = null;
    }

    m_UserdataXMLService = null;
    m_LogMarker = null;
    m_BundleContext = null;
  }//deactivate

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof RndStringGeneratorService) {
            m_RndStringGeneratorService = (RndStringGeneratorService) o;
            m_RndStringGeneratorLatch.countDown();
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = (MessageResourceService) o;
            m_MessageResourceServiceLatch.countDown();
          } else if (o instanceof MailService) {
            m_MailService = (MailService) o;
            m_MailServiceLatch.countDown();
          } else if (o instanceof DataSourceService) {
            m_DataSourceService = (DataSourceService) o;
            m_DataSourceServiceLatch.countDown();
          } else if (o instanceof StAXFactoryService) {
            m_StAXFactoryService = (StAXFactoryService) o;
            m_StAXFactoryServiceLatch.countDown();
          } else {
            m_BundleContext.ungetService(sr);
          }

          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof RndStringGeneratorService) {
            m_RndStringGeneratorService = null;
            m_RndStringGeneratorLatch = createWaitLatch();
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = null;
            m_MessageResourceServiceLatch = createWaitLatch();
          } else if (o instanceof MailService) {
            m_MailService = null;
            m_MailServiceLatch = createWaitLatch();
          } else if (o instanceof DataSourceService) {
            m_DataSourceService = null;
            m_DataSourceServiceLatch = createWaitLatch();
          } else if (o instanceof StAXFactoryService) {
            m_StAXFactoryService = null;
            m_StAXFactoryServiceLatch = createWaitLatch();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;

}//class ServiceMediator
