/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.userdata.model.UserdataPrivacy;
import net.coalevo.userdata.model.EditableUserdataPrivacy;

import java.util.BitSet;

/**
 * Implements {@link UserdataPrivacy} based on a BitSet.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class UserdataPrivacyImpl
    implements UserdataPrivacy {

  protected BitSet m_AccessBits;

  public UserdataPrivacyImpl() {
    m_AccessBits = new BitSet(15);
    m_AccessBits.set(0, 14, false);
  }//UserdataPrivacyImpl

  public UserdataPrivacyImpl(boolean b) {
    m_AccessBits = new BitSet(15);
    m_AccessBits.set(0, 14, b);
  }//UserdataPrivacyImpl

  public boolean getAllowsPrefix() {
    return m_AccessBits.get(0);
  }//getAllowsPrefix

  public boolean getAllowsFirstnames() {
    return m_AccessBits.get(1);
  }//getAllowsFirstnames

  public boolean getAllowsAdditionalnames() {
    return m_AccessBits.get(2);
  }//getAllowsAdditionalNames

  public boolean getAllowsLastnames() {
    return m_AccessBits.get(3);
  }//getAllowsLastnames

  public boolean getAllowsSuffix() {
    return m_AccessBits.get(4);
  }//getAllowsFirstnames

  public boolean getAllowsBirthdate() {
    return m_AccessBits.get(5);
  }//getAllowsBirthdate

  public boolean getAllowsGender() {
    return m_AccessBits.get(14);
  }//getAllowsGender

  public boolean getAllowsStreet() {
    return m_AccessBits.get(6);
  }//getAllowsStreet

  public boolean getAllowsExtension() {
    return m_AccessBits.get(7);
  }//getAllowsExtension

  public boolean getAllowsCity() {
    return m_AccessBits.get(8);
  }//getAllowsCity

  public boolean getAllowsCountry() {
    return m_AccessBits.get(9);
  }//getAllowsCountry

  public boolean getAllowsPostalCode() {
    return m_AccessBits.get(10);
  }//getAllowsPostalCode

  public boolean getAllowsEmail() {
    return m_AccessBits.get(11);
  }//getAllowsEmail

  public boolean getAllowsPhone() {
    return m_AccessBits.get(12);
  }//getAllowsPhone

  public boolean getAllowsMobile() {
    return m_AccessBits.get(13);
  }//getAllowsMobile

  public EditableUserdataPrivacy toEditableUserdataPrivacy() {
    EditableUserdataPrivacyImpl edp = new EditableUserdataPrivacyImpl();
    edp.m_AccessBits = (BitSet) this.m_AccessBits.clone();
    return edp;
  }//toEditableUserdataPrivacy

  /**
    * Defines a {@link UserdataPrivacy} that allows access to all
    * fields.
    */
   public static final UserdataPrivacy PUBLIC_ALL = new UserdataPrivacyImpl(true);

   /**
    * Defines a {@link UserdataPrivacy} that does not allow access to
    * any field.
    */
   public static UserdataPrivacy PRIVATE_ALL = new UserdataPrivacyImpl(false);


}//class UserdataPrivacyImpl
