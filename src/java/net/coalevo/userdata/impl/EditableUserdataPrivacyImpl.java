/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.userdata.model.UserdataPrivacy;
import net.coalevo.userdata.model.EditableUserdataPrivacy;

import java.util.BitSet;

/**
 * Implements {@link UserdataPrivacy}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EditableUserdataPrivacyImpl
    extends UserdataPrivacyImpl
    implements EditableUserdataPrivacy{

  public EditableUserdataPrivacyImpl() {
  }//EdiatbleUserdataPrivacyImpl

  public EditableUserdataPrivacyImpl(boolean b) {
    super(b);
  }//EditableUserdataPrivacyImpl

  public void setAllowsPrefix(boolean b) {
    m_AccessBits.set(0,b);
  }//setAllowsPrefix

  public void setAllowsFirstnames(boolean b) {
    m_AccessBits.set(1,b);
  }//setAllowsFirstnames

  public void setAllowsAdditionalnames(boolean b) {
    m_AccessBits.set(2,b);
  }//setAllowsAdditionalNames

  public void setAllowsLastnames(boolean b) {
    m_AccessBits.set(3,b);
  }//setAllowsLastnames

  public void setAllowsSuffix(boolean b) {
    m_AccessBits.set(4,b);
  }//setAllowsSuffix

  public void setAllowsBirthdate(boolean b) {
    m_AccessBits.set(5,b);
  }//setAllowsBirthdate

  public void setAllowsGender(boolean b) {
    m_AccessBits.set(14,b);
  }//setAllowsGender

  public void setAllowsStreet(boolean b) {
    m_AccessBits.set(6,b);
  }//setAllowsStreet

  public void setAllowsExtension(boolean b) {
    m_AccessBits.set(7,b);
  }//setAllowsExtension

  public void setAllowsCity(boolean b) {
    m_AccessBits.set(8,b);
  }//setAllowsCity

  public void setAllowsCountry(boolean b) {
    m_AccessBits.set(9,b);
  }//setAllowsCountry

  public void setAllowsPostalCode(boolean b) {
    m_AccessBits.set(10,b);
  }//setAllowsPostalCode

  public void setAllowsEmail(boolean b) {
    m_AccessBits.set(11,b);
  }//setAllowsEmail

  public void setAllowsPhone(boolean b) {
    m_AccessBits.set(12,b);
  }//setAllowsPhone

  public void setAllowsMobile(boolean b) {
    m_AccessBits.set(13,b);
  }//setAllowsMobile
  
  public UserdataPrivacyImpl toUserdataPrivacy() {
    UserdataPrivacyImpl udp = new UserdataPrivacyImpl();
    udp.m_AccessBits = (BitSet) this.m_AccessBits.clone();
    return udp;
  }//toUserdataPrivacyImpl

}//class EditableUserdataPrivacyImpl
