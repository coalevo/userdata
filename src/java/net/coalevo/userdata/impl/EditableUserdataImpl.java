/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.userdata.model.EditableUserdata;
import net.coalevo.userdata.model.EditableUserdataPrivacy;
import net.coalevo.text.util.MarkupLanguageFilter;

import java.util.Collections;
import java.util.Date;

/**
 * Provides an implementation of {@link EditableUserdata}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EditableUserdataImpl
    extends UserdataImpl
    implements EditableUserdata {

  private boolean m_NeedsConfirmation = false;
  private boolean m_NeedsReview = false;

  public EditableUserdataImpl(String uid) {
    m_Identifier = uid;
    m_Privacy = new EditableUserdataPrivacyImpl();
  }//constructor

  public EditableUserdataImpl() {
    m_Privacy = new EditableUserdataPrivacyImpl();
  }//constructor

  public void setIdentifier(String uid) {
    m_Identifier = uid;
  }//setIdentifier

  public void setFirstLoginDate(Date firstLoginDate) {
    m_FirstLoginDate = firstLoginDate;
  }//setFirstLoginDate

  public void setNickname(String nickname) {
    m_Nickname = checkUpdateAndReview(m_Nickname,nickname,true);
  }//setNickname

  public void setGender(String gender) {
    if (GENDER_MALE.equalsIgnoreCase(gender)) {
      gender = GENDER_MALE;
    } else if (GENDER_FEMALE.equalsIgnoreCase(gender)) {
      gender = GENDER_FEMALE;
    } else {
      gender = GENDER_UNKNOWN;
    }
    m_Gender = checkUpdateAndReview(m_Gender,gender,true);
  }//setGender

  public void setPhotoType(String photoType) {
    m_PhotoType = photoType;
  }//setPhotoType

  public void setInfo(String info) {
    m_Info = checkUpdateAndReview(m_Info,info,true);
  }//setInfo

  public void setInfoFormat(String format) {
    m_InfoFormat = format;
  }//setInfoFormat

  public void setAnnotations(String annotations) {
    m_Annotations = MarkupLanguageFilter.filterMLTags(annotations);
  }//setAnnotations

  public void setPrefix(String prefix) {
    m_Prefix = checkUpdateAndReview(m_Prefix,prefix,true);
  }//setPrefix

  public void setSuffix(String suffix) {
    m_Suffix = checkUpdateAndReview(m_Suffix,suffix,true);
  }//setSuffix

  public void setAdditionalNames(String additionalNames) {
    m_AdditionalNames = checkUpdateAndReview(m_AdditionalNames,additionalNames,true);
  }//setAdditionalNames

  public void setFirstnames(String firstnames) {
    m_Firstnames = checkUpdateAndReview(m_Firstnames,firstnames,true);
  }//setFirstnames

  public void setLastnames(String lastnames) {
    m_Lastnames = checkUpdateAndReview(m_Lastnames,lastnames,true);
  }//setLastnames

  public void setBirthdate(Date birthdate) {
    if(m_Birthdate != null && m_Birthdate.equals(birthdate)) {
      return;
    }
    updateReviewed();
    m_Birthdate = birthdate;
  }//setBirthdate

  public void setStreet(String street) {
    m_Street = checkUpdateAndReview(m_Street,street,true);
  }//setStreet

  public void setExtension(String extension) {
    m_Extension = checkUpdateAndReview(m_Extension,extension,true);
  }//setExtension

  public void setCity(String city) {
    m_City = checkUpdateAndReview(m_City, city, true);
  }//setCity

  public void setCountry(String country) {
    m_Country = checkUpdateAndReview(m_Country,country,true);
  }//setCountry

  public void setPostalCode(String postalCode) {
    m_PostalCode = checkUpdateAndReview(m_PostalCode,postalCode,true);
  }//setPostalCode

  public void setEmail(String email) {
    if(m_Email != null && m_Email.equals(email)) {
      return;
    }
    m_Email = email;
    if(m_Confirmed) {
      m_Confirmed = false;
      m_NeedsConfirmation = true;
    }
  }//setEmail

  public void setLinkType(String linkType) {
    if (LINK_HOMEPAGE.equalsIgnoreCase(linkType)) {
      m_LinkType = LINK_HOMEPAGE;
    } else if (LINK_BLOG.equalsIgnoreCase(linkType)) {
      m_LinkType = LINK_BLOG;
    } else {
      m_LinkType = LINK_OTHER;
    }
  }//setLinkType

  public void setLink(String link) {
    //should be validated as link by the frontend
    m_Link = checkUpdateAndReview(m_Link,link,false);
  }//setLink

  public void setPhone(String phone) {
    m_Phone = checkUpdateAndReview(m_Phone,phone,true);
  }//setPhone

  public void setMobile(String mobile) {
    m_Mobile = checkUpdateAndReview(m_Mobile,mobile,true);
  }//setMobile

  public void setLanguages(String languages) {
    m_Languages = checkUpdateAndReview(m_Languages,languages,false);
  }//setLanguage

  public void addFlag(String flag) {
    if (flag != null && flag.length() > 0 && !m_Flags.contains(flag)) {
      m_Flags.add(MarkupLanguageFilter.filterMLTags(flag));
    }
  }//addFlag

  public void updateFlag(int idx, String flag) {
    if(idx > m_Flags.size()) {
     throw new IndexOutOfBoundsException();
    }
    idx = idx - 1;
    if(idx < 0) {
      throw new IndexOutOfBoundsException();
    }
    m_Flags.set(idx,flag);
  }//updateFlag

  public void removeFlag(String flag) {
    m_Flags.remove(flag);
  }//removeFlag

  public boolean removeFlag(int idx) {
    if(idx > m_Flags.size()) {
      return false;
    }
    idx = idx - 1;
    if(idx < 0) {
      return false;
    }
    m_Flags.remove(idx);
    return true;
  }//removeFlag

  public void clearFlags() {
    m_Flags.clear();
  }//clearFlags

  public void setPhoto(String photo) {
    if(photo != null && photo.length() > 0 && photo.equals(m_Photo)) {
      return;
    }
    m_Photo = photo;
    updateReviewed();
  }//setPhoto

  public void setReviewed(boolean b) {
    m_Reviewed = b;
  }//setReviewed

  public void setConfirmed(boolean b) {
    m_Confirmed = b;
  }//setConfirmed

  public EditableUserdataPrivacy getEditablePrivacy() {
    return (EditableUserdataPrivacy)m_Privacy;
  }//getPrivacy

  public UserdataImpl toUserdata() {
    UserdataImpl ud = new UserdataImpl();
    ud.m_AdditionalNames = this.m_AdditionalNames;
    ud.m_Annotations = this.m_Annotations;
    ud.m_Birthdate = this.m_Birthdate;
    ud.m_City = this.m_City;
    ud.m_Country = this.m_Country;
    ud.m_Email = this.m_Email;
    ud.m_Extension = this.m_Extension;
    ud.m_FirstLoginDate = this.m_FirstLoginDate;
    ud.m_Firstnames = this.m_Firstnames;
    ud.m_Flags = Collections.unmodifiableList(this.m_Flags);
    ud.m_Gender = this.m_Gender;
    ud.m_Identifier = this.m_Identifier;
    ud.m_Info = this.m_Info;
    ud.m_InfoFormat = this.m_InfoFormat;
    ud.m_Languages = this.m_Languages;
    ud.m_Lastnames = this.m_Lastnames;
    ud.m_Link = this.m_Link;
    ud.m_LinkType = this.m_LinkType;
    ud.m_Mobile = this.m_Mobile;
    ud.m_Nickname = this.m_Nickname;
    ud.m_Phone = this.m_Phone;
    ud.m_Photo = this.m_Photo;
    ud.m_PhotoType = this.m_PhotoType;
    ud.m_PostalCode = this.m_PostalCode;
    ud.m_Prefix = this.m_Prefix;
    ud.m_Privacy = ((EditableUserdataPrivacyImpl)this.m_Privacy).toUserdataPrivacy();
    ud.m_Street = this.m_Street;
    ud.m_Suffix = this.m_Suffix;
    ud.m_Confirmed = this.m_Confirmed;
    ud.m_Reviewed = this.m_Reviewed;
    return ud;
  }//toUserdata

  public UserdataImpl toPublicUserdata() {
    UserdataImpl ud = new UserdataImpl();
    if (m_Privacy.getAllowsAdditionalnames()) {
      ud.m_AdditionalNames = this.m_AdditionalNames;
    }
    ud.m_Annotations = null;
    if (m_Privacy.getAllowsBirthdate()) {
      ud.m_Birthdate = this.m_Birthdate;
    }
    if (m_Privacy.getAllowsCity()) {
      ud.m_City = this.m_City;
    }
    if (m_Privacy.getAllowsCountry()) {
      ud.m_Country = this.m_Country;
    }
    if (m_Privacy.getAllowsEmail()) {
      ud.m_Email = this.m_Email;
    }
    if (m_Privacy.getAllowsExtension()) {
      ud.m_Extension = this.m_Extension;
    }
    ud.m_FirstLoginDate = this.m_FirstLoginDate;
    if (m_Privacy.getAllowsFirstnames()) {
      ud.m_Firstnames = this.m_Firstnames;
    }
    ud.m_Flags = Collections.unmodifiableList(this.m_Flags);

    if (m_Privacy.getAllowsGender()) {
      ud.m_Gender = this.m_Gender;
    }
    ud.m_Identifier = this.m_Identifier;

    ud.m_Info = this.m_Info;
    ud.m_Languages = this.m_Languages;
    if (m_Privacy.getAllowsLastnames()) {
      ud.m_Lastnames = this.m_Lastnames;
    }
    ud.m_Link = this.m_Link;
    ud.m_LinkType = this.m_LinkType;

    if (m_Privacy.getAllowsMobile()) {
      ud.m_Mobile = this.m_Mobile;
    }
    ud.m_Nickname = this.m_Nickname;
    if (m_Privacy.getAllowsPhone()) {
      ud.m_Phone = this.m_Phone;
    }
    ud.m_Photo = this.m_Photo;
    ud.m_PhotoType = this.m_PhotoType;
    if (m_Privacy.getAllowsPostalCode()) {
      ud.m_PostalCode = this.m_PostalCode;
    }
    if (m_Privacy.getAllowsPrefix()) {
      ud.m_Prefix = this.m_Prefix;
    }
    ud.m_Privacy = ((EditableUserdataPrivacyImpl)this.m_Privacy).toUserdataPrivacy();
    if (m_Privacy.getAllowsStreet()) {
      ud.m_Street = this.m_Street;
    }
    if (m_Privacy.getAllowsSuffix()) {
      ud.m_Suffix = this.m_Suffix;
    }
    ud.m_Confirmed = this.m_Confirmed;
    ud.m_Reviewed = this.m_Reviewed;
    ud.m_Public = true;
    return ud;
  }//toPublicUserdata
  
  public boolean isConfirmationNeeded() {
    return m_NeedsConfirmation;
  }//isConfirmationNeeded

  public boolean isReviewNeeded() {
    return m_NeedsReview;
  }//isReviewNeeded

  private String checkUpdateAndReview(String act, String str, boolean filter) {
    if(filter) {
      str = MarkupLanguageFilter.filterMLTags(str);
    }
    if(!str.equals(act)) {
      updateReviewed();
      return str;
    } else {
      return act;
    }
  }//checkUpdateAndReview

  private void updateReviewed() {
    if(m_Reviewed) {
      m_Reviewed = false;
      m_NeedsReview = true;
    }
  }//updateReview

}//class EditableUserdataImpl
