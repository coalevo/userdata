/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.foundation.model.Maintainable;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.BundleConfiguration;
import net.coalevo.logging.model.LogProxy;
import net.coalevo.userdata.service.UserdataConfiguration;
import net.coalevo.userdata.service.UserdataService;
import net.coalevo.userdata.service.UserdataXMLService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;

/**
 * Provides a <tt>BundleActivator</tt> implementation for
 * the userdata bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private static LogProxy c_Log;
  private static Marker c_LogMarker;

  private static ServiceMediator c_Services;
  private static Messages c_BundleMessages;

  private static UserdataStore c_UserdataStore;
  private static File c_UserdataIndexPath;
  private static UserdataServiceImpl c_UserdataService;
  private static BundleConfiguration c_BundleConfiguration;

  private Thread m_StartThread;

  public void start(final BundleContext bundleContext)
      throws Exception {
    if(m_StartThread!=null && m_StartThread.isAlive()) {
      throw new Exception();
    }
    m_StartThread = new Thread(
        new Runnable() {
          public void run() {
            try {
              //1. Prepare log
              c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
              c_Log = new LogProxy();
              c_Log.activate(bundleContext);

              //2. Services
              c_Services = new ServiceMediator();
              c_Services.activate(bundleContext);

              //3. Bundle Messages
              c_BundleMessages =
                  c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED)
                      .getBundleMessages(bundleContext.getBundle());

              //4. Bundle Configuration
              c_BundleConfiguration = new BundleConfiguration(UserdataConfiguration.class.getName());
              c_BundleConfiguration.activate(bundleContext);
              c_Services.setConfigMediator(c_BundleConfiguration.getConfigurationMediator());

              //5. Userdata XML Service (required for store)
              UserdataXMLService udxs = new UserdataXMLServiceImpl();
              if (!udxs.activate(bundleContext)) {
                log().error(c_BundleMessages.get("Activator.activation.exception", "service", "UserdataXMLService"));
                return;
              }
              bundleContext.registerService(
                  UserdataXMLService.class.getName(),
                  udxs,
                  null);
              log().debug(c_BundleMessages.get("Activator.activation.service", "service", "UserdataXMLService"));
              c_Services.setUserdataXMLService(udxs);

              //6. Userdata Store
              c_UserdataIndexPath = new File(bundleContext.getDataFile(""), "ud_store");
              log().debug(c_BundleMessages.get("Activator.activation.store"));
              c_UserdataStore = new UserdataStore();
              c_UserdataStore.activate();

              //7. Userdata Service
              c_UserdataService = new UserdataServiceImpl(c_UserdataStore);
              if (!c_UserdataService.activate(bundleContext)) {
                log().error(c_BundleMessages.get("Activator.activation.exception", "service", "UserdataService"));
                return;
              }
              String[] classes = {UserdataService.class.getName(), Maintainable.class.getName()};
              bundleContext.registerService(
                  classes,
                  c_UserdataService,
                  null);
              log().debug(c_BundleMessages.get("Activator.activation.service", "service", "UserdataService"));
            } catch (Exception ex) {
              log().error(c_LogMarker, "start(BundleContext)", ex);
            }

          }//run
        }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(this.getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext)
      throws Exception {

    //wait start
    if (m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }

    if (c_UserdataService != null) {
      c_UserdataService.deactivate();
      c_UserdataService = null;
    }
    if (c_UserdataStore != null) {
      c_UserdataStore.deactivate();
      c_UserdataStore = null;
    }
    if (c_BundleConfiguration != null) {
      c_BundleConfiguration.deactivate();
    }
    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }
    if (c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }

    c_UserdataIndexPath = null;
    c_LogMarker = null;
    c_BundleMessages = null;
  }//stop

  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  public static File getUserdataIndexPath() {
    return c_UserdataIndexPath;
  }//getUserdataIndexPath

  /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

}//class Activator
