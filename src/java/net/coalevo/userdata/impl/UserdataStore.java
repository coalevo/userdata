/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.userdata.service.UserdataConfiguration;
import net.coalevo.datasource.service.DataSourceService;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.Library;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.NoSuchElementException;
import java.util.HashMap;

/**
 * Implements an RDBMS based store for userdata.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class UserdataStore implements ConfigurationUpdateHandler {

  private Marker m_LogMarker = MarkerFactory.getMarker(UserdataStore.class.getName());

  private Library m_SQLLibrary;
  private DataSource m_DataSource;
  private GenericObjectPool m_ConnectionPool;

  //Concurrency handling for backup and restore
  private CountDownLatch m_LeaseLatch;
  private AtomicInteger m_NumLeased = new AtomicInteger(0);
  private CountDownLatch m_RestoreLatch;

  private UserdataInstanceCache m_UserdataCache;
  private LRUCacheMap<AgentIdentifier, String> m_NicknameCache;
  private Messages m_BundleMessages;

  private int m_ConfirmationExpiry;

  public UserdataStore() {

  }//constructor

  /**
   * Lease a connection to the underlying database.
   *
   * @return a {@link LibraryConnection} instance.
   * @throws Exception if the connection pool fails or a connection cannot be created.
   */
  public LibraryConnection leaseConnection()
      throws Exception {
    //If a lease latch is set, wait for countdown
    if (m_LeaseLatch != null) {
      m_LeaseLatch.await();
    }
    LibraryConnection lc = (LibraryConnection) m_ConnectionPool.borrowObject();
    m_NumLeased.addAndGet(1);
    return lc;
  }//leaseConnection

  public void releaseConnection(LibraryConnection lc) {
    try {
      m_ConnectionPool.returnObject(lc);
      if (m_NumLeased.decrementAndGet() == 0 && m_RestoreLatch != null) {
        m_RestoreLatch.countDown();
      }
    } catch (Exception e) {
      Activator.log().error(m_LogMarker,"releaseConnection()", e);
    }
  }//releaseConnection

  public UserdataInstanceCache getUserdataCache() {
    return m_UserdataCache;
  }//getUserdataCache

  public LRUCacheMap<AgentIdentifier, String> getNicknameCache() {
    return m_NicknameCache;
  }//getNicknameCache

  public int getConfirmationExpiry() {
    return m_ConfirmationExpiry;
  }//getConfirmationExpiry

  private synchronized void prepareDataSource() {
    LibraryConnection lc = null;
    try {
      lc = leaseConnection();
      //1. check select
      try {
        lc.executeQuery("existsSchema", null);
      } catch (SQLException ex) {
        createSchema(lc);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"prepareDataSource()", ex);
    } finally {
      releaseConnection(lc);
    }
  }//prepareDataSource

  private boolean createSchema(LibraryConnection lc) {
    try {
      lc = leaseConnection();
      //1. Create schema, tables and index
      lc.executeCreate("createUserdataSchema");
      lc.executeCreate("createUserdataTable");
      lc.executeCreate("createNicknameIndex");
      lc.executeCreate("createConfirmationTable");
      return true;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"createDatabase()", ex);
    } finally {
      releaseConnection(lc);
    }
    return false;
  }//createDatabase

  public boolean activate() {
    m_BundleMessages = Activator.getBundleMessages();
    //1. Configure from persistent configuration
    String ds = "embedded";
    int cpoolsize = 2;
    int udcachesize = 25;
    int nickcachesize = 50;

    ConfigurationMediator cm = Activator.getServices().getConfigMediator();
    MetaTypeDictionary mtd = cm.getConfiguration();

     try {
      ds = mtd.getString(UserdataConfiguration.DATA_SOURCE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataStore.activation.configexception", "attribute", UserdataConfiguration.DATA_SOURCE_KEY),
          ex
      );
    }
    try {
      cpoolsize = mtd.getInteger(UserdataConfiguration.CONNECTIONPOOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataStore.activation.configexception", "attribute", UserdataConfiguration.CONNECTIONPOOLSIZE_KEY),
          ex
      );
    }
    try {
      udcachesize = mtd.getInteger(UserdataConfiguration.USERDATACACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataStore.activation.configexception", "attribute", UserdataConfiguration.USERDATACACHESIZE_KEY),
          ex
      );
    }
    try {
      nickcachesize = mtd.getInteger(UserdataConfiguration.NICKNAMECACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataStore.activation.configexception", "attribute", UserdataConfiguration.USERDATACACHESIZE_KEY),
          ex
      );
    }
    try {
      m_ConfirmationExpiry = mtd.getInteger(UserdataConfiguration.CONFIRM_EXPIRY_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      m_ConfirmationExpiry = 48;
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataStore.activation.configexception", "attribute", UserdataConfiguration.CONFIRM_EXPIRY_KEY),
          ex
      );
    }

    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = cpoolsize;
    poolcfg.maxIdle = cpoolsize;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolcfg.testOnBorrow = true;
    m_ConnectionPool = new GenericObjectPool(new ConnectionFactory(), poolcfg);

    //prepare SQL library and datasource
    try {
      m_SQLLibrary =
          new Library(UserdataStore.class, "net/coalevo/userdata/impl/userdatastore-sql.xml");
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"activate()", ex);
      return false;
    }
    DataSourceService dss = Activator.getServices().getDataSourceService(ServiceMediator.WAIT_UNLIMITED);
    try {
      m_DataSource = dss.waitForDataSource(ds, -1);
    } catch (NoSuchElementException nse) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataStore.activation.datasource"),
          nse
      );
      return false;
    }
    //prepare store
    prepareDataSource();
    Activator.log().info(m_LogMarker,
        m_BundleMessages.get("UserdataStore.database.info", "source", m_DataSource.toString())
    );


    //create caches
    m_UserdataCache = new UserdataInstanceCache(UserdataStore.this, udcachesize, Activator.getServices().getUserdataXMLService());
    m_NicknameCache = new LRUCacheMap<AgentIdentifier, String>(nickcachesize);

    //add handler config updates
    cm.addUpdateHandler(this);

    return true;
  }//activate

  public synchronized boolean deactivate() {
    //don't bother
    //m_SecurityService.invalidateAuthentication(m_ServiceAgent);
    //1. close all leased connections?
    if (m_ConnectionPool != null) {
      try {
        m_ConnectionPool.close();
      } catch (Exception e) {
        Activator.log().error(m_LogMarker,"deactivate()", e);
      }
    }
    //2. clear caches?
    if (m_UserdataCache != null) {
      m_UserdataCache.clear();
    }
    m_DataSource = null;
    m_SQLLibrary = null;
    m_ConnectionPool = null;
    m_BundleMessages = null;
    return true;
  }//deactivate

  /**
   * Backups the database of userdata while running.
   * The database will be frozen for writes, but no interruption
   * for reads is required.
   *
   * @param f   the directory to write the backup to.
   * @param tag a possible tag for the backup.
   * @throws SecurityException if not authorized to backup.
   * @throws BackupException if the backup fails.
   */
  public void backup(File f, String tag)
      throws BackupException {

    /*

    //prepare file
    File backup = null;
    if (tag != null && tag.length() > 0) {
      backup = new File(f, tag);
    } else {
      backup = f;
    }
    //Get connection
    Connection conn = null;
    try {
      conn = m_DataSource.getConnection();
    } catch (SQLException e) {
      Activator.log().error("doBackup()", e);
      throw new BackupException("UserdataStore:: Could not obtain db connection.", e);
    }

    //1. Backup Database
    try {
      CallableStatement cs =
          conn.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)");
      cs.setString(1, backup.getAbsolutePath());
      cs.execute();
      cs.close();
      Activator.log().info(
          m_BundleMessages.get("UserdataStore.backup.done", "path",
              backup.getAbsolutePath())
      );
    } catch (Exception ex) {
      Activator.log().error("doBackup()", ex);
    }
    */
  }//doBackup

  public void restore(File f, String tag)
      throws RestoreException {
    /*
    //1. Latch for future leases
    m_RestoreLatch = new CountDownLatch(1);
    m_LeaseLatch = new CountDownLatch(1);

    try {
      m_RestoreLatch.await();
    } catch (InterruptedException ex) {
      Activator.log().error("restore()", ex);
    }

    //prepare file
    File bdir = new File(f, UserdataStore.class.getName());
    if (!bdir.exists()) {
      throw new RestoreException("UserdataStore:: Failed obtain directory of backup.");
    }
    File backup = null;
    if (tag != null && tag.length() > 0) {
      backup = new File(bdir, tag);
    } else {
      backup = bdir;
    }
    //Restore from connection
    //Connection conn = null;
    m_DataSource.setConnectionAttributes(";restoreFrom=" + backup.getAbsolutePath());

    try {
      maintain();
    } catch (MaintenanceException ex) {
      Activator.log().error("restore()", ex);
    } finally {
      m_DataSource.setConnectionAttributes("");
    }

    m_RestoreLatch = null;
    m_LeaseLatch.countDown();
    m_LeaseLatch = null;
    Activator.log().info(m_BundleMessages.get("UserdataStore.restore.done"));
    */
  }//restore

  public void maintain(Agent agent) throws MaintenanceException {

    //Database maintainance
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();

    LibraryConnection lc = null;
    try {

      lc = leaseConnection();
      //1. purge unconfirmed entries
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.unconfirmed"));
      long expiry = System.currentTimeMillis() - (m_ConfirmationExpiry * 3600000); //hours => millis
      params.put("expired", Long.toString(expiry));
      rs = lc.executeQuery("listUnconfirmed",params);
      while(rs.next()) {
        //Purge confirmation request entry and the stale user data.
        params.clear();
        params.put("agent_id", rs.getString(1));
        try {
          lc.executeUpdate("removeConfirmationID",params);
          lc.executeUpdate("removeUnconfirmedUserdata",params);
          //unregister agent from security service
          Activator.getServices().getSecurityManagementService().unregisterAgent(
              agent,
              new AgentIdentifier(params.get("agent_id"))
              );
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.unconfirmed", "aid", params.get("agent_id")));
        }

      }

    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.store"),ex);
    } finally {
      SqlUtils.close(rs);
      releaseConnection(lc);
    }

    //clear caches
    Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.caches"));
    m_NicknameCache.clear();
    m_UserdataCache.clear();
  }//maintain

  public void update(MetaTypeDictionary mtd) {
    ///TODO: Datadir
    try {
      final int cpsize = mtd.getInteger(UserdataConfiguration.CONNECTIONPOOLSIZE_KEY).intValue();
      m_ConnectionPool.setMaxActive(cpsize);
      m_ConnectionPool.setMaxIdle(cpsize);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataStore.activation.configexception", "attribute", UserdataConfiguration.CONNECTIONPOOLSIZE_KEY),
          ex
      );
    }
    try {
      m_UserdataCache.setCacheSize(mtd.getInteger(UserdataConfiguration.USERDATACACHESIZE_KEY).intValue());
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataStore.activation.configexception", "attribute", UserdataConfiguration.USERDATACACHESIZE_KEY),
          ex
      );
    }
    try {
      m_NicknameCache.setCeiling(mtd.getInteger(UserdataConfiguration.NICKNAMECACHESIZE_KEY).intValue());
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("UserdataStore.activation.configexception", "attribute", UserdataConfiguration.USERDATACACHESIZE_KEY),
          ex
      );
    }

  }//update

  private class ConnectionFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      Connection c = m_DataSource.getConnection();
      c.setAutoCommit(true);
      return new LibraryConnection(m_SQLLibrary, c);
    }//makeObject

    public boolean validateObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      try {
        lc.executeQuery("validate",null);
      } catch (Exception ex) {
        return false;
      }
      return true;
    }//validateObject

    public void destroyObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      if (!lc.isClosed()) {
        SqlUtils.close(lc);
      }
    }//destroyObject

  }//ConnectionFactory


}//class UserdataStore
