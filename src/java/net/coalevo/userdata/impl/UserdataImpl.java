/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.userdata.impl;

import net.coalevo.userdata.model.Userdata;
import net.coalevo.userdata.model.UserdataPrivacy;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Provides a very simple implementation of {@link Userdata}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class UserdataImpl implements Userdata {

  //top level
  protected String m_Identifier;
  protected Date m_FirstLoginDate;
  protected String m_Nickname;
  protected String m_Photo;
  protected String m_Info;
  protected String m_InfoFormat;
  protected String m_Annotations;
  protected List<String> m_Flags = new ArrayList<String>();
  //personal
  protected String m_Prefix;
  protected String m_Suffix;
  protected String m_AdditionalNames;
  protected String m_Firstnames;
  protected String m_Lastnames;
  protected Date m_Birthdate;
  //address
  protected String m_Street;
  protected String m_Extension;
  protected String m_City;
  protected String m_Country;
  protected String m_PostalCode;
  //communications
  protected String m_Email;
  protected String m_Link;
  protected String m_Phone;
  protected String m_Mobile;
  protected String m_Languages;
  //types
  protected String m_PhotoType;
  protected String m_LinkType;
  protected String m_Gender;

  protected boolean m_Confirmed = false;
  protected boolean m_Reviewed = false;

  protected boolean m_Public = false;

  protected UserdataPrivacyImpl m_Privacy;

  /**
   * Private constructor, so it cannot be constructed.
   * Use Editable and transform it to a userdata instance.
   */
  protected UserdataImpl() {
  }

  public UserdataPrivacy getPrivacy() {
    return m_Privacy;
  }//getPrivacy

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public Date getFirstLoginDate() {
    return m_FirstLoginDate;
  }//getFirstLoginDate

  public String getNickname() {
    return m_Nickname;
  }//getNickname

  public String getGender() {
    return m_Gender;
  }//getGender

  public String getPhotoType() {
    return m_PhotoType;
  }//getPhotoType

  public String getPhoto() {
    return m_Photo;
  }//getPhoto

  public boolean hasPhoto() {
    return (m_Photo != null && m_PhotoType != null);
  }//hasPhoto

  public String getInfo() {
    return m_Info;
  }//getInfo

  public String getInfoFormat() {
    return m_InfoFormat;
  }//getInfoFormat

  public String getAnnotations() {
    return m_Annotations;
  }//getAnnotations

  public String getPrefix() {
    return m_Prefix;
  }//getPrefix

  public String getSuffix() {
    return m_Suffix;
  }//getSuffix

  public String getAdditionalnames() {
    return m_AdditionalNames;
  }//getAdditionalnames

  public String getFirstnames() {
    return m_Firstnames;
  }//getFirstnames

  public String getLastnames() {
    return m_Lastnames;
  }//getLastnames

  public Date getBirthdate() {
    return m_Birthdate;
  }//getBirthdate

  public String getStreet() {
    return m_Street;
  }//getStreet

  public String getExtension() {
    return m_Extension;
  }//getExtension

  public String getCity() {
    return m_City;
  }//getCity

  public String getCountry() {
    return m_Country;
  }//getCountry

  public String getPostalCode() {
    return m_PostalCode;
  }//getPostalCode

  public String getEmail() {
    return m_Email;
  }//getEmail

  public String getLink() {
    return m_Link;
  }//getLink

  public String getPhone() {
    return m_Phone;
  }//getPhone

  public String getMobile() {
    return m_Mobile;
  }//getMobile

  public String getLanguages() {
    return m_Languages;
  }//getLanguage

  public Iterator<String> getFlags() {
    return m_Flags.iterator();
  }//getFlags

  public String getFlag(int idx) {
    if (idx > m_Flags.size()) {
      throw new IndexOutOfBoundsException();
    }
    idx = idx - 1;
    if (idx < 0) {
      throw new IndexOutOfBoundsException();
    }
    return m_Flags.get(idx);
  }//getFlag

  public int getNumberOfFlags() {
    return (m_Flags == null) ? 0 : m_Flags.size();
  }//getNumberOfFlags

  public String getLinkType() {
    return m_LinkType;
  }//getLinkType

  public boolean isConfirmed() {
    return m_Confirmed;
  }//isConfirmed

  public boolean isReviewed() {
    return m_Reviewed;
  }//isReviewed

  public boolean isPublic() {
    return m_Public;
  }//isPublic

  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    final Userdata userdata = (Userdata) o;

    if (m_Identifier != null ? !m_Identifier.equals(userdata.getIdentifier()) : userdata.getIdentifier() != null)
      return false;

    return true;
  }//equals

  public int hashCode() {
    return (m_Identifier != null ? m_Identifier.hashCode() : 0);
  }//hashCode

}//class UserdataImpl
